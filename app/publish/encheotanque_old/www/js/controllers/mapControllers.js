angular.module('fuel.controllers')

.controller('MapController', MapController)
.controller('MapRouteController', MapRouteController);

function MapController($scope, $rootScope, $state, $stateParams, $ionicPlatform, Enterprises, Device, ionicToast) {

	$scope.$on('$ionicView.enter', function(e){
		$rootScope.toggledrag = false;
	});

	Device.setStatus('LOADING');

	var marker = [];

	$scope.mapCreated = function(map) {
      
      $scope.map = map;

    };

	var successCallback = function(position) {

      Device.getGeocode(position.coords.latitude, position.coords.longitude,
      function(results){
        if (results[1]) {

          ionicToast.show(results[0].formatted_address, 'bottom', false, 2500);

          for(i = 0; i < results.length; i++){

            if(results[i].types[0] == 'locality'){

              $scope.bounds     = results[i].geometry.bounds.toUrlValue();
              $rootScope.lat    = position.coords.latitude;
              $rootScope.lon    = position.coords.longitude;

              $scope.allStationsMap();

			  var my_pos = new google.maps.LatLng($rootScope.lat, $rootScope.lon);
		      $scope.map.setCenter(my_pos);

		      // Adiciona um marcador no mapa
		      var myLocation = new google.maps.Marker({
		            position: new google.maps.LatLng($rootScope.lat, $rootScope.lon),
		            map: $scope.map,
		            title: "Estou Aqui"
		      });

            }

          }

        } else {
          Device.setStatus('POSITION_ERROR');
          ionicToast.show('Erro ao obter a posição do dispositivo!', 'bottom', false, 2500);
        }
      },
      function(status){
        Device.setStatus('NET_ERROR');
        ionicToast.show('Verifique sua conexão com a internet!', 'bottom', false, 2500);
      });

	}

	$scope.allStationsMap = function(){

	    Enterprises.allMap($scope.bounds, $rootScope.lat, $rootScope.lon).then(
	    function(success){
	      
	      Device.resetStatus();
	      
	      // Realiza diversas verificações e monta os campos compostos e booleanos
	      stations = Enterprises.resolveStations(success.data.data);

	      // Adiciona os marcadores dos postos no mapa 
	      for(var index in stations) { 
			
			var attr = stations[index];
			// Adiciona um marcador no mapa
			var image = {url : $rootScope.url_base + '/mydash/upload/flag/' + attr.flag_img ,
			 			 scaledSize: new google.maps.Size(32, 32),
			 			 origin: new google.maps.Point(0, 0),
   						 anchor: new google.maps.Point(0, 0)};

		    marker[i] = new google.maps.Marker({
		            position: new google.maps.LatLng(attr.latitude, attr.longitude),
		            map: $scope.map,
		            title: attr.name,
		            icon: image,
		            stationId : attr.id,
		            distance : attr.distance,
		            view_name : attr.name
		    });

		    marker[i].addListener('click', function() {
			    $state.go('station',{stationId:this.stationId, distance: this.distance, view_name: this.view_name})
			 });

		  }

	    },
	    function(error){
	      evice.setStatus('NET_ERROR');
	      ionicToast.show('Verifique sua conexão com a internet!', 'bottom', false, 2500); 
	    });

	  }

    	
	$scope.resetStations = function(){

		Device.resetStatus();
		Device.setStatus('LOADING');

    	if(navigator.connection.type != Connection.UNKNOWN && navigator.connection.type != Connection.NONE){

	      	if(window.google && window.google.maps){

		        console.log('Achei o google');
		        Device.getPosition(successCallback, function(e){
		          Device.setStatus('POSITION_ERROR');
		          ionicToast.show('Erro ao obter a posição do dispositivo!', 'bottom', false, 2500);
		        })

		    }else{
		          
		        console.log('Não achei o google');
		        require(['async!https://maps.googleapis.com/maps/api/js?key=AIzaSyC7rPUi1XEP-0T-P7Eb2Poq-mQ6CEFgaoQ&sensor=true'], function() {
		  
		          Device.getPosition(successCallback, function(e){
		            Device.setStatus('POSITION_ERROR');
		            ionicToast.show('Erro ao obter a posição do dispositivo!', 'bottom', false, 2500);
		          })
		  
		        });

		    }

	    }else{

	      Device.setStatus('NET_ERROR');
	      ionicToast.show('Verifique sua conexão com a internet!', 'bottom', false, 2500);
	      $scope.$broadcast('scroll.refreshComplete');

	    }

	}

  	$ionicPlatform.ready(function(){
		$scope.resetStations();
	});
  
}

function MapRouteController($scope, $rootScope, $state, $stateParams, $ionicHistory) {

	$scope.$on('$ionicView.enter', function(e){
		$rootScope.toggledrag = false;
	});

	$scope.mapCreated = function(map) {
      
      $scope.map = map;
      var directionsDisplay;
      var my_pos = new google.maps.LatLng($rootScope.lat, $rootScope.lon);
	  var promo_pos = new google.maps.LatLng($stateParams.lat, $stateParams.lon);

      directionsService = new google.maps.DirectionsService();
      directionsDisplay = new google.maps.DirectionsRenderer();
      directionsDisplay.setMap($scope.map);

      $scope.map.setCenter(my_pos);

      // Adiciona um marcador no mapa
      /*var myLocation = new google.maps.Marker({
            position: new google.maps.LatLng($rootScope.lat, $rootScope.lon),
            map: map,
            title: "Estou Aqui"
       });*/
	  
	  // Traça o caminho entre o usuário e a promoção - modo DIRIGINDO
	  var selectedMode = 'DRIVING';
	  var request = {
	      origin: my_pos,
	      destination: promo_pos,
	      travelMode: google.maps.TravelMode[selectedMode]
	  };
	  directionsService.route(request, function(response, status) {
	    if (status == google.maps.DirectionsStatus.OK) {
	      directionsDisplay.setDirections(response);
	    }
	  });

    };

    $scope.closeMap = function(){


    	$ionicHistory.goBack(-1);

    }

}