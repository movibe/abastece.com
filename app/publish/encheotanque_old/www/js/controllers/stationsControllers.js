angular.module('fuel.controllers')

.controller('StationsController', StationsController)

.controller('StationController', StationController);

function StationsController($scope, $rootScope, $ionicModal, $ionicPlatform, Enterprises, Device, Requisition, ionicToast) {

  $scope.$on('$ionicView.enter', function(e){
    $rootScope.toggledrag = true;
  });

  Device.setStatus('LOADING');

  $scope.sort_selector = 'distance';
  $scope.sort  = $scope.sort_selector;
  $scope.temp_sort  = $scope.sort;
  $scope.sort_description = 'Distância';

  $scope.fuel_selector = 'common';
  $scope.fuel = $scope.fuel_selector;
  $scope.temp_fuel = $scope.fuel;
  $scope.fuel_description = 'Comuns';

  $scope.payment_selector = '0'; // Todos
  $scope.payment = $scope.payment_selector;
  $scope.temp_payment = $scope.payment;
  $scope.payment_description = 'Todos';

  var successCallback = function(position) {

      Device.getGeocode(position.coords.latitude, position.coords.longitude,
      function(results){
        if (results[1]) {

          ionicToast.show(results[0].formatted_address, 'bottom', false, 2500);

          for(i = 0; i < results.length; i++){

            if(results[i].types[0] == 'locality'){

              $scope.bounds     = results[i].geometry.bounds.toUrlValue();
              $rootScope.lat    = position.coords.latitude;
              $rootScope.lon    = position.coords.longitude;

              $scope.allStations();

            }

          }

        } else {
          Device.setStatus('POSITION_ERROR');
          ionicToast.show('Erro ao obter a posição do dispositivo!', 'bottom', false, 2500);
        }
      },
      function(status){
        Device.setStatus('NET_ERROR');
        ionicToast.show('Verifique sua conexão com a internet!', 'bottom', false, 2500);
      });

  }

  $scope.resetStations = function(){

    Device.resetStatus();
    Device.setStatus('LOADING');

    if(navigator.connection.type != Connection.UNKNOWN && navigator.connection.type != Connection.NONE){

      if(window.google && window.google.maps){

        console.log('Achei o google');
        Device.getPosition(successCallback, function(e){
          Device.setStatus('POSITION_ERROR');
          ionicToast.show('Erro ao obter a posição do dispositivo!', 'bottom', false, 2500);
        })

      }else{
          
        console.log('Não achei o google');
        require(['async!https://maps.googleapis.com/maps/api/js?key=AIzaSyAZjumNWX0PIZ-1d3JFrX9gtIesHKfnC4E&sensor=true'], function() {
  
          Device.getPosition(successCallback, function(e){
            Device.setStatus('POSITION_ERROR');
            ionicToast.show('Erro ao obter a posição do dispositivo!', 'bottom', false, 2500);
          })
  
        });

      }

    }else{

      Device.setStatus('NET_ERROR');
      ionicToast.show('Verifique sua conexão com a internet!', 'bottom', false, 2500);
      $scope.$broadcast('scroll.refreshComplete');

    }

  }

  $ionicPlatform.ready(function(){
    $scope.resetStations();
  });

  $scope.allStations = function(){


    Enterprises.all($scope.bounds, $rootScope.lat, $rootScope.lon).then(
    function(success){
      
      // Realiza diversas verificações e monta os campos compostos e booleanos
      $scope.stations = Enterprises.resolveStations(success.data.data);
      Device.resetStatus();
      
      if($scope.stations.length <= 0){
        Device.setStatus('NOTHING');
        $scope.filtered_stations = {};
      }else{

        // Realiza a filtragem dos postos
        $scope.filtered_stations = Enterprises.filterStations($scope.fuel, $scope.payment, $scope.stations);

      }

      $scope.$broadcast('scroll.refreshComplete');

    },
    function(error){
      Device.setStatus('NET_ERROR');
      $scope.$broadcast('scroll.refreshComplete');
      ionicToast.show('Verifique sua conexão com a internet!', 'bottom', false, 2500); 
    });

  }

  $ionicModal.fromTemplateUrl('templates/modal-search-stations.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.openSearch = function() {
    $scope.payments = Device.getPayments();
    $scope.modal.show();
  };
  $scope.closeSearch = function() {
    $scope.modal.hide();
  };

  $scope.setSort = function(sort){
    // Seta a ordenação temporária, que será repassada apenas se a pesquisa for confirmada
    $scope.temp_sort = sort;
  }

  $scope.setFuel = function(fuel){
    // Seta o combustível que será filtrado
    $scope.temp_fuel = fuel;
  }

  $scope.setPayment = function(payment){
    // Seta a forma de pagamento que será filtrada
    $scope.temp_payment = payment;
  }

  $scope.setSearch = function(){

    Device.resetStatus();

    $scope.sort = $scope.temp_sort;
    $scope.sort_description = Enterprises.sortDescription($scope.temp_sort);

    $scope.fuel = $scope.temp_fuel;
    $scope.fuel_description = Enterprises.fuelDescription($scope.temp_fuel);

    $scope.payment = $scope.temp_payment;
    $scope.payment_description = Device.paymentDescription($scope.temp_payment);

    // Realiza a filtragem das estações
    $scope.filtered_stations = Enterprises.filterStations($scope.fuel, $scope.payment, $scope.stations);

    $scope.closeSearch();

  }

  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });

  /*$scope.canShow = function(station){

    return Enterprises.canShow($scope.fuel, $scope.payment, station);

  }*/

}

function StationController($scope, $rootScope, $stateParams, $state, $ionicModal, $ionicHistory, Device, Enterprises, Users, ionicToast) {

  Device.setStatus('LOADING');

  $scope.station_view_name = $stateParams.view_name;

  $scope.$on('$ionicView.enter', function(e){
    $rootScope.toggledrag = true;
    // Verifica se possui algum usuário local e carrega-o
    Users.verifyLocalUser();
    
    // Verifica os comentários do usuário
    user_id = Users.getLocalUserId();
    if($scope.station) $scope.station.my_comment = Enterprises.getMyComment($scope.station.comments, user_id);

  })

  Enterprises.get($stateParams.stationId).then(function(s){

    $scope.station = Enterprises.resolveStation(s.data.data, $stateParams.distance);
    $scope.station.comments = Users.resolveComments($scope.station.comments);

    user_id = Users.getLocalUserId();
    $scope.station.my_comment = Enterprises.getMyComment($scope.station.comments, user_id);

    Device.resetStatus();

    // Busca os serviços
    Enterprises.getServices($stateParams.stationId).then(function(s){

      $scope.station.services = s.data.data;

    }, function(e){

      // Não mostrar esse erro para o usuário
      console.log('Erro ao buscar os serviços do posto!!');

    })

  }, function(e){

    Device.setStatus('NET_ERROR');
    ionicToast.show('Verifique a conexão de internet.', 'bottom', false, 2500);

  })

  $scope.shownItem = null;
  $scope.toggleItem = function(item) {
    if ($scope.isItemShown(item)) {
      $scope.shownItem = null;
    } else {
      $scope.shownItem = item;
    }
  };
  $scope.isItemShown = function(item) {
    return $scope.shownItem === item;
  };

  $scope.openMap = function(){

    $state.go('map-route', {lat : $scope.station.latitude, lon : $scope.station.longitude});

  }

  // Abre a chamada do celular caso exista um número que possa ser chamado
  $scope.call = function(tel) {
    if(tel){
      tel = '0' + tel.replace(/[^a-zA-Z0-9 ]/g, "");
      window.location.href = 'tel:'+ tel;
    }else{
      ionicToast.show('Esse posto não possui telefone cadastrado.', 'bottom', false, 2500);
    }
  }

  $ionicModal.fromTemplateUrl('templates/modal-classify.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.openClassify = function(mode) {

    $scope.classify = {'classification':1, 'comment' : ''};

    if(mode == 'classify'){

      $scope.mode_classify = 'classify';

      $scope.showComments = false;
      $scope.class_view_name = 'Classificação e Comentário';
      $scope.user = {'can_send_email':true};
      $scope.modal.show();

    }
    else
    if(mode == 'edit'){

      $scope.mode_classify = 'edit';

      $scope.showComments = false;
      $scope.class_view_name = 'Editar Classificação';
      $scope.classify.id = $scope.station.my_comment.id;
      $scope.classify.classification = $scope.station.my_comment.classification;
      $scope.classify.comment = $scope.station.my_comment.text;
      $scope.modal.show();

    }
    else{

      $scope.mode_classify = 'comments';

      $scope.showComments = true;
      $scope.class_view_name = 'Comentários deste Posto';
      $scope.modal.show();

    }

  };

  $scope.closeClassify = function() {
    $scope.modal.hide();
  };

  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });

  $scope.sendUser = function(create, user){

    if(Users.validateFields(create,user)){
      
      // Vai criar uma nova conta
      if(create){

        user.active = true;
        Users.save(user).then(function(s){

          user_data = s.data;
          if(user_data.error){
            ionicToast.show('Já existe uma conta com esse email.', 'bottom', false, 2500);
          }else{
            Users.get(user_data.data.id).then(function(s){
              Users.saveLocal(s.data.data);
              ionicToast.show('Conta criada com sucesso.', 'bottom', false, 2500);
              Users.verifyLocalUser();
            },function(e){
              ionicToast.show('Erro ao buscar usuário no servidor.', 'bottom', false, 2500);
            })
          }

        }, function(e){

          ionicToast.show('Ocorreu um erro ao salvar esse usuário.', 'bottom', false, 2500);

        })

      }else{

        Users.login(user.email, user.password).then(function(s){

          data = s.data;
          
          if(data.error){

            ionicToast.show('Usuário não encontrado.', 'bottom', false, 2500);

          }else{

            user = data.data;
            delete user.api_token; // Não vamos precisar desse token pois já temos do dispositivo
            Users.saveLocal(user);
            ionicToast.show('Conta sincronizada com sucesso.', 'bottom', false, 2500);
            Users.verifyLocalUser();

            // Seta o meu comentário
            $scope.mode_classify = 'edit';
            $scope.class_view_name = 'Editar Classificação';
            $scope.station.my_comment = Enterprises.getMyComment($scope.station.comments, user.id);
            $scope.classify.id = $scope.station.my_comment.id;
            $scope.classify.classification = $scope.station.my_comment.classification;
            $scope.classify.comment = $scope.station.my_comment.text;

          }

        },function(e){

          ionicToast.show('Problemas ao sincronizar sua conta. Tente novamente.', 'bottom', false, 2500);

        })

      }

    }else{

      ionicToast.show('Confira e Preencha todos os campos.', 'bottom', false, 2500);

    }

  } 

  $scope.ratingsObject = {
    iconOn : 'ion-ios-star',
    iconOff : 'ion-ios-star-outline',
    iconOnColor: 'rgb(255,208,0)',
    iconOffColor:  'rgb(220, 220, 220)',
    callback: function(rating) {
      $scope.ratingsCallback(rating);
    }
  };

  $scope.ratingsCallback = function(rating) {

    $scope.classify.classification = rating;

  };

  $scope.sendRating = function(classify) {

    Users.sendRating($scope.station.id, $rootScope.local_user.id, classify, $scope.mode_classify).then(function(s){

      ionicToast.show('Seu comentário foi enviado com sucesso.', 'bottom', false, 2500);
      $scope.station.comments = Users.resolveComments(s.data.data);
      if($scope.mode_classify != 'edit') $scope.station.comments_number++;
      $scope.station.my_comment = Enterprises.getMyComment($scope.station.comments, $rootScope.local_user.id);
      $scope.closeClassify();

    }, function(e){

      ionicToast.show('Falha ao enviar seu comentário.', 'bottom', false, 2500);

    });

  }

  $scope.deleteRating = function(){

    $scope.isDeleting = true;

    if(confirm('Deseja deletar seu comentário?')){

      Users.deleteRating($scope.station.my_comment.id).then(function(s){

        if(s.data.error){
          ionicToast.show('Falha ao deletar seu comentário. Tente novamente.', 'bottom', false, 2500);
        }else{
          ionicToast.show('Deletado com sucesso.', 'bottom', false, 2500);
          $scope.station.comments = Users.resolveComments(s.data.data);
          $scope.station.comments_number--;
          $scope.station.my_comment = null;
        }

        $scope.isDeleting = false;

      },function(e){

        $scope.isDeleting = false;
        ionicToast.show('Falha ao deletar seu comentário. Tente novamente.', 'bottom', false, 2500);

      })

    }

  }

  $scope.closeDetail = function(){

    $ionicHistory.goBack(-1);

  }

}