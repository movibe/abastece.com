angular.module('fuel.controllers', []);

angular.module('fuel.controllers')

.controller('HomeController', HomeController);

function HomeController($scope, $rootScope, $ionicSideMenuDelegate, ionicToast, Users) {

	// Verifica se possui algum usuário local e carrega-o
  	Users.verifyLocalUser();

	$scope.showMenu = function () {
	   $ionicSideMenuDelegate.toggleLeft();
	};

	$scope.logout = function(){

		Users.logout();
		ionicToast.show('Logout efetuado com sucesso.', 'bottom', false, 2500);

	}

	$scope.about = function(){

		openPage('http://vidaapp.com.br/encheotanque/sobre.html')

	}

	$scope.share = function(){

		/*$cordovaSocialSharing
	    .share(message, subject, file, link) // Share via native share sheet
	    .then(function(result) {
	      // Success!
	    }, function(err) {
	      // An error occured. Show a message to the user
	    });*/

	}

}