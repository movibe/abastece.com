// Funções úteis

// Transforma um timestamp mysql em Date()
function mysqlTimeToDate(date){
	
	t = date.split(/[- :]/)
    d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);

    return d;

}

// Formata uma data mysql para um input
function mysqlTimeToInput(date){

    var separate = date.split(' ');

    var my_date  = separate[0].split('-');
    var my_time  = separate[1].split(':');

    return (my_date[2] + '/' + my_date[1] + '/' + my_date[0] + ' ' + my_time[0] + ':' + my_time[1]);

}

// Diferença entre datas
function myDiff(date1,date2,interval) {
    var second=1000, minute=second*60, hour=minute*60, day=hour*24, week=day*7;
    date1 = new Date(date1);
    date2 = new Date(date2);
    var timediff = date2 - date1;
    if (isNaN(timediff)) return NaN;
    switch (interval) {
        case "years": return date2.getFullYear() - date1.getFullYear();
        case "months": return (
            ( date2.getFullYear() * 12 + date2.getMonth() )
            -
            ( date1.getFullYear() * 12 + date1.getMonth() )
        );
        case "weeks"  : return Math.floor(timediff / week);
        case "days"   : return Math.floor(timediff / day); 
        case "hours"  : return Math.floor(timediff / hour); 
        case "minutes": return Math.floor(timediff / minute);
        case "seconds": return Math.floor(timediff / second);
        default: return undefined;
    }
}

function openPage(page){

    window.open(page, '_blank', 'location=no');

}