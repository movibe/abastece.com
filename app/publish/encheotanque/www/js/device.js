angular.module('fuel.services')

.factory('Device', function($rootScope, $http, Requisition){

  savePayments = function(data){

    window.localStorage['encheotanque_payments'] = angular.toJson(data);

  }

  myPosition = function(successCallback, errorCallback, options){

    navigator.geolocation.getCurrentPosition(
    successCallback, 
    errorCallback, 
    options);

  }

  return {

    register : function(){
      
      return $http.post(Requisition.getFullURL('devices') + '/register');

    },

    setStatus : function(status){

      switch(status){
        case 'LOADING':
          $rootScope.inform_loading = true;
          break;
        case 'NOTHING': // O servidor não retornou nada
          $rootScope.inform_loading = false;
          $rootScope.inform_nothing = true;
          $rootScope.inform_error   = true;
          break;
        case 'FILTER_ERROR': // Não foi encontrado nada na filtragem especificada
          $rootScope.inform_loading      = false;
          $rootScope.inform_filter_error = true;
          break;
        case 'POSITION_ERROR': // A posição não pode ser encontrada
          $rootScope.inform_loading        = false;
          $rootScope.inform_position_error = true;
          $rootScope.inform_error          = true;
          break;
        case 'NET_ERROR': // Houve um erro de conexão com a internet
          $rootScope.inform_loading   = false;
          $rootScope.inform_net_error = true;
          $rootScope.inform_error     = true;
          break;
        default: // Se nada foi especificado, reseta o status
          this.resetStatus();
          break;
      }

    },

    // Reseta o estado atual do dispositivo
    resetStatus : function(){

      $rootScope.inform_error           = false;
      $rootScope.inform_loading         = false;
      $rootScope.inform_nothing         = false;
      $rootScope.inform_filter_error    = false;
      $rootScope.inform_position_error  = false;
      $rootScope.inform_net_error       = false;

    },

    payments : function(){
      
      return $http.get(Requisition.getFullURL('payments') + '/mobile');

    },

    getPayments : function(){

      return angular.fromJson(window.localStorage['encheotanque_payments']);

    },

    paymentDescription : function(id){

      if(id == '0'){
        return 'Todos';
      }else{

        payments = this.getPayments();

        for(i = 0; i < payments.length; i++){
          if(payments[i].id == id) return payments[i].name;
        }

      }

    return '';

    },

    addressPosition: function(address){

      street = address.replace(' ', '+');
      street = street.replace(',', '+');
      street = street.replace(':', '+');

      url = 'https://maps.google.com/maps/api/geocode/json?address='+street+'&sensor=false';

      return $http({
          method: 'GET',
          url: url,
          // Aqui deletamos os headers para utilizar essa requisição externa
          transformRequest: function(data, headersGetter) {
              var headers = headersGetter();

              delete headers['ident'];
              delete headers['token'];
              delete headers['type'];

              return headers;
          }
      })

    },

    resolveAddress: function(data){

      hasLocation = false;

      if(data.results.length > 0){
            
            loc = data.results[0].geometry.location;

            // Salva no dispositivo a última posição
            window.localStorage['busqueoff_lat_default'] = loc.lat;
            window.localStorage['busqueoff_lon_default'] = loc.lng;

            $rootScope.lat  = loc.lat;
            $rootScope.lon  = loc.lng;

            console.log('Coordenadas encontradas')

            hasLocation = true;
          
        }else{

            $rootScope.lat  = null;
            $rootScope.lon  = null;

            console.log('Não existem coordenadas')

            hasLocation = false;

        }

        return hasLocation;

    },

    getPosition : function(successCallback, errorCallback){

      var options = {
        enableHighAccuracy: true,
        timeout: 7000,
        maximumAge: 120000
      };

      myPosition(successCallback,function(error){
        
        console.log('Buscar Posição: Baixa Precisão');
        // Tenta buscar a posição novamente, mas desliga a alta precisão
        var options = {
          enableHighAccuracy: false,
          timeout: 7000,
          maximumAge: 120000
        };
        
        myPosition(successCallback, errorCallback, options);

      }, options);

    },

    // Obtém o endereço baseado na posição
    getGeocode : function (_lat, _lon, successCallback, errorCallback) {
      
      var geocoder = new google.maps.Geocoder;
      var latlng = {lat: _lat, lng: _lon};
      
      geocoder.geocode({'location': latlng}, function(results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
          
          successCallback(results);

        } else {
          
          errorCallback(status);

        }
      });

    },
    
    setHeadersAndPayments : function(){

      // Verifica se o dispositivo já está registrado
      if(!window.localStorage['encheotanque_device_id'] || !window.localStorage['encheotanque_device_token']){

        this.register().then(
          function(success){

            data = success.data;

            if(!data.error){
              
              device = data.data;
              window.localStorage['encheotanque_device_id'] = device.id;
              window.localStorage['encheotanque_device_token'] = device.api_token;

              $http.defaults.headers.common.ident = window.localStorage['encheotanque_device_id'];
              $http.defaults.headers.common.token = window.localStorage['encheotanque_device_token'];
              $http.defaults.headers.common.type = 'DEVICE';

            }else{

              alert('Impossível registrar o dispositivo no servidor!');

            }

          },
          function(error){
            alert('Registro de Dispositivo: \n Verifique se você está conectado com a internet!');
          }
        )

      }else{

        $http.defaults.headers.common.ident = window.localStorage['encheotanque_device_id'];
        $http.defaults.headers.common.token = window.localStorage['encheotanque_device_token'];
        $http.defaults.headers.common.type = 'DEVICE';

        // Busca as formas de pagamento
        this.payments().then(
          function(success){
            savePayments(success.data.data)
          },
          function(error){
            console.log('Não consegui obter os pagamentos');
          });

      }

    }

  }

})