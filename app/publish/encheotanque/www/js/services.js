angular.module('fuel.services', [])

.factory('Users', function($rootScope, $http, Requisition){

  return {

    all : function(max, offset, desc){
      
      Requisition.setModel('user');
      return Requisition.all(max, offset, desc);

    },

    get : function(id){

      Requisition.setModel('user');
      return Requisition.get(id);

    },

    save : function(data){

      Requisition.setModel('user');
      return Requisition.save(data);
      

    },

    saveLocal : function(data){

      window.localStorage['encheotanque_user'] = angular.toJson(data);

    },

    getLocal : function(){

      user = window.localStorage['encheotanque_user'];

      return angular.fromJson(user);

    },

    // Carrega um usuário no escopo global caso ele exista
    verifyLocalUser : function(){

      user = this.getLocal();

      if(user) $rootScope.local_user = user;

    },

    getLocalUserId : function(){

      if($rootScope.local_user) return $rootScope.local_user.id;

      return 0;

    },

    logout : function(){

      window.localStorage['encheotanque_user'] =  null;
      $rootScope.local_user = null;

    },

    validateFields : function(create, user){

      if(!user) return false;

      if(user.password != user.re_password) return false;

      if(create){

        if(user.name && user.email && user.password) return true;

      }else{

        if(user.email && user.password) return true;

      }

      return false;

    },

    delete : function(id){

      Requisition.setModel('user');
      return Requisition.delete(id);

    },

    login : function(email, password){
      
      return $http.post(Requisition.getFullURL('user') + '/login/', 
      {'email' : email, 'password' : password});

    },

    sendRating : function(station_id, user_id, classify, mode){
      
      if(mode == 'classify'){

        return $http.post(Requisition.getFullURL('user') + '/' + user_id + '/enterprise/' + station_id + '/rate', 
        classify);

      }else{

        return $http.put(Requisition.getFullURL('user') + '/rate/edit', 
        classify);

      }

    },

    deleteRating : function(id){

      return $http.delete(Requisition.getFullURL('user') + '/rate/' + id);

    },

    resolveComments : function(data){

      data.map(function(el){

        el.updated_at = mysqlTimeToInput(el.updated_at);

      });

      return data;

    }

  }

})

.factory('Enterprises', function($rootScope, $http, Requisition) {

  return {

    all : function(bounds, lat, lon, limit, offset, description){

      Requisition.setModel('enterprise');

      limit       = (limit) ? limit : '';
      offset      = offset ? offset : '';
      description = (description) ? description : '';
      bounds      = bounds ? bounds : '';
      lat         = lat ? lat : '';
      lon         = lon ? lon : '';

      return $http.get(Requisition.getURL() + 'v1/enterprises/mobile?description=' 
        + description + '&limit=' + limit + '&skip=' + offset
        + '&bounds=' + bounds + '&lat=' + lat + '&lon=' + lon);

    },

    allMap : function(bounds, lat, lon, limit, offset, description){

      Requisition.setModel('enterprise');

      limit       = (limit) ? limit : '';
      offset      = offset ? offset : '';
      description = (description) ? description : '';
      bounds      = bounds ? bounds : '';
      lat         = lat ? lat : '';
      lon         = lon ? lon : '';

      return $http.get(Requisition.getURL() + 'v1/enterprises/mobile/map?limit=' + limit + '&skip=' + offset
        + '&bounds=' + bounds + '&lat=' + lat + '&lon=' + lon);

    },

    get : function(id){

      Requisition.setModel('enterprise');
      return $http.get(Requisition.getURL() + 'v1/enterprise/' + id + '/mobile');

    },

    getServices : function(id){

      Requisition.setModel('enterprise');
      return $http.get(Requisition.getURL() + 'v1/enterprise/' + id + '/services');

    },

    save : function(data){

      Requisition.setModel('enterprise');
      return Requisition.save(data);
      
    },

    delete : function(id){

      Requisition.setModel('enterprise');
      return Requisition.delete(id);

    },

    resolveStations : function(data){

      if(data){
        
        data.map(function(el){
  
          // Monta o endereço
          el.address = el.street + ', ' + el.number + ' - ' + el.district + ' / ' + el.city;
  
          el.hasGasoline = (el.hasGasoline == 1) ? true : false;
          el.hasGasolineLeaded = (el.hasGasolineLeaded == 1) ? true : false;
          el.hasGasolinePremium = (el.hasGasolinePremium == 1) ? true : false;
          el.hasEthanol = (el.hasEthanol == 1) ? true : false;
          el.hasDiesel = (el.hasDiesel == 1) ? true : false;
          el.hasDieselS10 = (el.hasDieselS10 == 1) ? true : false;
          el.hasNaturalGas = (el.hasNaturalGas == 1) ? true : false;
  
          // Tratamento dos preços
          el.priceGasoline = (el.hasGasoline && (el.priceGasoline > 0)) ? el.priceGasoline : '-----'; 
          el.priceGasolineLeaded = (el.hasGasolineLeaded && (el.priceGasolineLeaded > 0)) ? el.priceGasolineLeaded : '-----'; 
          el.priceGasolinePremium = (el.hasGasolinePremium && (el.priceGasolinePremium > 0)) ? el.priceGasolinePremium : '-----'; 
          el.priceEthanol = (el.hasEthanol && (el.priceEthanol > 0)) ? el.priceEthanol : '-----'; 
          el.priceDiesel = (el.hasDiesel && (el.priceDiesel > 0)) ? el.priceDiesel : '-----'; 
          el.priceDieselS10 = (el.hasDieselS10 && (el.priceDieselS10 > 0)) ? el.priceDieselS10 : '-----'; 
          el.priceNaturalGas = (el.hasNaturalGas && (el.priceNaturalGas > 0)) ? el.priceNaturalGas : '-----'; 
  
        })
  
        return data;

      }else{

        return {};

      }

    },

    filterStations : function(fuel, payment, stations){

      var filtered=[];

      if(stations){

        for(var i=0;i<stations.length;i++){
          if(this.canShow(fuel, payment, stations[i])){
            filtered.push(stations[i]);
          }
        }

      }

      return filtered;

    },

    resolveStation : function(data, distance){

      if(data){
      
        data.opinions = (data.totalClassified == 0 || data.totalClassified > 1) ? data.totalClassified + ' opiniões' :
        data.totalClassified + ' opinião';
  
        data.hasGasoline = (data.hasGasoline == 1) ? true : false;
        data.hasGasolineLeaded = (data.hasGasolineLeaded == 1) ? true : false;
        data.hasGasolinePremium = (data.hasGasolinePremium == 1) ? true : false;
        data.hasEthanol = (data.hasEthanol == 1) ? true : false;
        data.hasDiesel = (data.hasDiesel == 1) ? true : false;
        data.hasDieselS10 = (data.hasDieselS10 == 1) ? true : false;
        data.hasNaturalGas = (data.hasNaturalGas == 1) ? true : false;

        data.operation = (data.time_initial) ? 'Aberto das ' + data.time_initial + ' às ' + data.time_final : null;
  
        data.distance = distance;
        data.address = data.street + ', ' + data.number + ' - ' + data.district + ' / ' + data.city;
  
        date = mysqlTimeToDate(data.fuel_updated_at);
        now  = new Date;
  
        data.update_diff = myDiff(date, now, 'days'); 
  
        return data;
        
      }else{

        return {};

      }

    },

    sortDescription : function(sort){

      switch(sort){
        case 'distance':
          return 'Distância';
          break;
        case 'priceDiesel':
          return 'Preço do Diesel';
          break;
        case 'priceDieselS10':
          return 'Preço do Diesel S10';
          break;
        case 'priceEthanol':
          return 'Preço do Etanol';
          break;
        case 'priceNaturalGas':
          return 'Preço do GNV';
          break;
        case 'priceGasoline':
          return 'Preço da Gasolina';
          break;
        case 'priceGasolineLeaded':
          return 'Preço da Gasolina Aditivada';
          break;
        case 'priceGasolinePremium':
          return 'Preço da Gasolina Premium';
          break;
        case 'name':
          return 'Posto (Nome)';
          break;
        case 'flag_fk':
          return 'Bandeira (Agrupar)';
          break;
        case '-classification':
          return 'Classificação';
          break;
        default:
          return '--';
          break;
      }
    },

    fuelDescription : function(fuel){

      switch(fuel){
        case 'common':
          return 'Comuns';
          break;
        case 'diesel':
          return 'Diesel';
          break;
        case 'diesel_s10':
          return 'Diesel S10';
          break;
        case 'ethanol':
          return 'Etanol';
          break;
        case 'gnv':
          return 'GNV';
          break;
        case 'gasoline':
          return 'Gasolina';
          break;
        case 'gasoline_leaded':
          return 'Gasolina Aditivada';
          break;
        case 'gasoline_premium':
          return 'Gasolina Premium';
          break;
        default:
          return '--';
          break;
      }
    },

    hasPayment : function(id, payments){

      // Caso seja igual a 0, ou seja, todas as formas de pagamento
      if(id == 0) return true;
      
      for(i = 0; i < payments.length; i++){ 

        if(payments[i].id == id) return true;

      }

      return false;

    },

    getMyComment : function(comments, user_id){

      if(user_id){

        for(i = 0; i < comments.length; i++){

          if(comments[i].person_fk == user_id) return comments[i];

        }

      }else{
        return null;
      }

      return null;

    },

    canShow : function(fuel, payment, station){

      // Testa primeiramente se possui as formas de pagamento
      has_payment = this.hasPayment(payment, station.payments);

      if(fuel == 'common'){

        if(station.hasGasoline || station.hasEthanol || station.hasDiesel || station.hasNaturalGas)
          return has_payment;

      }

      if(fuel == 'ethanol'){

        if(station.hasEthanol)
          return has_payment;

      }

      if(fuel == 'diesel'){

        if(station.hasDiesel)
          return has_payment;

      }

      if(fuel == 'diesel_s10'){

        if(station.hasDieselS10)
          return has_payment;

      }

      if(fuel == 'gasoline'){

        if(station.hasGasoline)
          return has_payment;

      }

      if(fuel == 'gasoline_leaded'){

        if(station.hasGasolineLeaded)
          return has_payment;

      }

      if(fuel == 'gasoline_premium'){

        if(station.hasGasolinePremium)
          return has_payment;

      }

      if(fuel == 'gnv'){

        if(station.hasNaturalGas)
          return has_payment;

      }

      return false;
      
    }

  }

});
