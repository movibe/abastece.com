// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('fuel', ['ionic', 'fuel.controllers', 'fuel.services', 'directives', 'requisition',
 'ionic-toast', 'ionic-ratings'])

.run(function($ionicPlatform, $rootScope, $http, Requisition, Device, Users) {

  //$rootScope.url_base = 'http://localhost:8080/abastece_com'; Requisition.setURL($rootScope.url_base + '/api/');
  //$rootScope.url_base = 'http://192.168.0.12:8080/abastece_com'; Requisition.setURL($rootScope.url_base + '/api/');
  $rootScope.url_base = 'http://vidaapp.com.br/encheotanque'; Requisition.setURL($rootScope.url_base + '/api/');

  $rootScope.toggledrag = true;

  // Seta os headers e carrega as formas de pagamento
  Device.setHeadersAndPayments();

  // Configura o requireJS
  require.config({
      paths: {
          'async': 'lib/requirejs/requirejs-plugins/src/async'
      }
  });

  $ionicPlatform.ready(function() {

    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleLightContent();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

  // Habilita o scroll nativo para android
  if(!ionic.Platform.isIOS())$ionicConfigProvider.scrolling.jsScrolling(false);

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })

  // Each tab has its own nav history stack:

  .state('tab.stations', {
    url: '/stations',
    views: {
      'tab-stations': {
        templateUrl: 'templates/tab-stations.html',
        controller: 'StationsController'
      }
    }
  })

  .state('station', {
    url: '/station/:stationId/:distance/:view_name',
    templateUrl: 'templates/detail-station.html',
    controller: 'StationController'
  })

  .state('map-route', {

    url: "/map-route/:lat/:lon",
    templateUrl : 'templates/page-map-route.html',
    controller  : 'MapRouteController'

  })

  .state('tab.map', {
    url: '/map',
    views: {
      'tab-map': {
        templateUrl: 'templates/tab-map.html',
        controller: 'MapController'
      }
    }
  })

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/stations');

});
