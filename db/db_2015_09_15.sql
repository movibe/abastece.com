-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           5.6.12-log - MySQL Community Server (GPL)
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Copiando estrutura para tabela abastece_com.comment
CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text NOT NULL,
  `enterprise_fk` int(11) NOT NULL,
  `person_fk` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `enteprise_fk3` (`enterprise_fk`),
  KEY `person_fk` (`person_fk`),
  CONSTRAINT `enteprise_fk3` FOREIGN KEY (`enterprise_fk`) REFERENCES `enterprise` (`id`),
  CONSTRAINT `person_fk` FOREIGN KEY (`person_fk`) REFERENCES `person` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela abastece_com.comment: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;


-- Copiando estrutura para tabela abastece_com.device
CREATE TABLE IF NOT EXISTS `device` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `notification_token` text,
  `api_token` varchar(40) DEFAULT NULL,
  `last_latitude` varchar(20) DEFAULT NULL,
  `last_longitude` varchar(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela abastece_com.device: ~7 rows (aproximadamente)
/*!40000 ALTER TABLE `device` DISABLE KEYS */;
INSERT INTO `device` (`id`, `notification_token`, `api_token`, `last_latitude`, `last_longitude`, `created_at`, `updated_at`) VALUES
	(1, NULL, 'fb15e1a6accedc31c9aef30872a5bd27fed6df13', NULL, NULL, '2015-09-09 17:18:03', '2015-09-09 17:18:03'),
	(2, NULL, '2bca65489d65bb53b7e3a100a01a2ab089727249', NULL, NULL, '2015-09-09 17:18:03', '2015-09-09 17:18:03'),
	(3, NULL, 'f3e56dedfe918d65a1510a41e32459a0f36e12b5', NULL, NULL, '2015-09-09 17:23:16', '2015-09-09 17:23:16'),
	(4, NULL, '78af97a2ce15caa180f0ee2be2f0a73a8cbcc716', NULL, NULL, '2015-09-10 09:50:57', '2015-09-10 09:50:57'),
	(5, NULL, '639dd0307368ba0d289754f3ea6489d29353f7dd', NULL, NULL, '2015-09-10 10:45:57', '2015-09-10 10:45:57'),
	(6, NULL, '4b36c5427b010157e880da0dd3187be9d5d09019', NULL, NULL, '2015-09-11 11:16:27', '2015-09-11 11:16:27'),
	(7, NULL, '237a8f9f79a11dc1efcbe9c9febef689d6f382a4', NULL, NULL, '2015-09-14 10:17:49', '2015-09-14 10:17:49');
/*!40000 ALTER TABLE `device` ENABLE KEYS */;


-- Copiando estrutura para tabela abastece_com.enterprise
CREATE TABLE IF NOT EXISTS `enterprise` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `description` varchar(64) DEFAULT NULL,
  `details` text,
  `flag_fk` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '0',
  `img` varchar(32) DEFAULT NULL,
  `totalStars` smallint(6) DEFAULT NULL COMMENT 'Total de estrelas da classificação',
  `totalClassified` smallint(6) DEFAULT NULL COMMENT 'Total de pessoas que classificaram',
  `classification` float DEFAULT NULL COMMENT 'Média da classificação',
  `comments` smallint(5) unsigned DEFAULT NULL,
  `inscription` varchar(64) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `phone` varchar(16) DEFAULT NULL,
  `url` varchar(128) DEFAULT NULL,
  `credit` double DEFAULT NULL,
  `facebook_url` varchar(128) DEFAULT NULL,
  `street` varchar(128) NOT NULL,
  `number` int(11) DEFAULT NULL,
  `complement` varchar(16) DEFAULT NULL,
  `postal` varchar(16) DEFAULT NULL,
  `district` varchar(32) DEFAULT NULL,
  `country` varchar(32) NOT NULL,
  `state` varchar(32) DEFAULT NULL,
  `city` varchar(64) NOT NULL,
  `latitude` float(13,10) DEFAULT NULL,
  `longitude` float(13,10) DEFAULT NULL,
  `time_initial` varchar(5) DEFAULT NULL,
  `time_final` varchar(5) DEFAULT NULL,
  `hasGasoline` tinyint(1) DEFAULT NULL,
  `hasGasolineLeaded` tinyint(1) DEFAULT NULL,
  `hasGasolinePremium` tinyint(1) DEFAULT NULL,
  `hasEthanol` tinyint(1) DEFAULT NULL,
  `hasDiesel` tinyint(1) DEFAULT NULL,
  `hasDieselS10` tinyint(1) DEFAULT NULL,
  `hasNaturalGas` tinyint(1) DEFAULT NULL,
  `priceGasoline` decimal(10,3) DEFAULT NULL,
  `priceGasolineLeaded` decimal(10,3) DEFAULT NULL,
  `priceGasolinePremium` decimal(10,3) DEFAULT NULL,
  `priceEthanol` decimal(10,3) DEFAULT NULL,
  `priceDiesel` decimal(10,3) DEFAULT NULL,
  `priceDieselS10` decimal(10,3) DEFAULT NULL,
  `priceNaturalGas` decimal(10,3) DEFAULT NULL,
  `fuel_updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `flag_fk` (`flag_fk`),
  CONSTRAINT `flag_fk` FOREIGN KEY (`flag_fk`) REFERENCES `flag` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela abastece_com.enterprise: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `enterprise` DISABLE KEYS */;
INSERT INTO `enterprise` (`id`, `name`, `description`, `details`, `flag_fk`, `active`, `img`, `totalStars`, `totalClassified`, `classification`, `comments`, `inscription`, `email`, `phone`, `url`, `credit`, `facebook_url`, `street`, `number`, `complement`, `postal`, `district`, `country`, `state`, `city`, `latitude`, `longitude`, `time_initial`, `time_final`, `hasGasoline`, `hasGasolineLeaded`, `hasGasolinePremium`, `hasEthanol`, `hasDiesel`, `hasDieselS10`, `hasNaturalGas`, `priceGasoline`, `priceGasolineLeaded`, `priceGasolinePremium`, `priceEthanol`, `priceDiesel`, `priceDieselS10`, `priceNaturalGas`, `fuel_updated_at`, `created_at`, `updated_at`) VALUES
	(1, 'Posto Ipiranga', 'sdfdsf', 'sddfsdfsdfsdf', 3, 1, 'logotype_132_1.jpg', NULL, NULL, NULL, NULL, '234234324324234234234', 'tiagosilvapereira3@gmail.com', '36537373', 'https://www.youtube.com/watch?v=Dp4wFv5fHL0', NULL, 'http://facebook.com', 'Antonio Aspix', 1123, NULL, '16012777', 'Nobreville', 'Brasil', 'São Paulo', 'Araçatuba', -21.2028541565, -50.4536781311, '07:00', '23:00', 1, 1, 0, 1, 1, 0, 1, 2.275, 2.897, 2.134, 1.450, 2.123, 456456.000, 5.897, NULL, '2015-09-04 12:08:21', '2015-09-10 16:46:06'),
	(2, 'Posto do Zé Mané', 'adasd', 'asdasd', 6, 1, 'logotype_399_2.jpg', NULL, NULL, NULL, NULL, NULL, 'tiagosilvapereira3@gmail.com', '345345', NULL, NULL, NULL, 'Antonio Sampaio', 1162, NULL, '16012777', 'Nobreville', 'Brasil', 'SP', 'Araçatuba', -21.2058734894, -50.3977699280, NULL, NULL, 1, 0, 0, 0, 1, 0, 0, 5.897, 0.000, 0.000, 0.000, 2.890, 0.000, 0.000, NULL, '2015-09-10 10:26:48', '2015-09-11 11:36:28'),
	(3, 'Abacadabra', NULL, NULL, 5, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Cussy Almeida', 234, NULL, '16012777', NULL, 'Brasil', 'São Paulo', 'Araçatuba', NULL, NULL, NULL, NULL, 1, 0, 0, 0, 1, 0, 0, 2.567, 0.000, 0.000, 0.000, 3.456, 0.000, 0.000, NULL, '2015-09-10 11:26:12', '2015-09-10 16:24:47'),
	(4, 'Cosa leenda', NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Cussy Almeida', 2345, NULL, '16012777', NULL, 'Brasil', 'São Paulo', 'Araçatuba', NULL, NULL, NULL, NULL, 1, 0, 0, 1, 0, 0, 0, 2.389, 0.000, 0.000, 1.456, 0.000, 0.000, 0.000, NULL, '2015-09-10 11:27:30', '2015-09-11 11:38:48');
/*!40000 ALTER TABLE `enterprise` ENABLE KEYS */;


-- Copiando estrutura para tabela abastece_com.enterprise_payment
CREATE TABLE IF NOT EXISTS `enterprise_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_fk` int(11) NOT NULL DEFAULT '0',
  `enterprise_fk` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `payment_fk` (`payment_fk`),
  KEY `enterprise_fk` (`enterprise_fk`),
  CONSTRAINT `enterprise_fk` FOREIGN KEY (`enterprise_fk`) REFERENCES `enterprise` (`id`),
  CONSTRAINT `payment_fk` FOREIGN KEY (`payment_fk`) REFERENCES `payment` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela abastece_com.enterprise_payment: ~11 rows (aproximadamente)
/*!40000 ALTER TABLE `enterprise_payment` DISABLE KEYS */;
INSERT INTO `enterprise_payment` (`id`, `payment_fk`, `enterprise_fk`, `created_at`, `updated_at`) VALUES
	(121, 2, 1, '2015-09-10 16:46:06', '2015-09-10 16:46:06'),
	(122, 4, 1, '2015-09-10 16:46:07', '2015-09-10 16:46:07'),
	(123, 6, 1, '2015-09-10 16:46:07', '2015-09-10 16:46:07'),
	(126, 2, 2, '2015-09-11 11:36:28', '2015-09-11 11:36:28'),
	(127, 4, 2, '2015-09-11 11:36:29', '2015-09-11 11:36:29'),
	(128, 6, 2, '2015-09-11 11:36:29', '2015-09-11 11:36:29'),
	(133, 3, 4, '2015-09-11 11:38:48', '2015-09-11 11:38:48'),
	(134, 4, 4, '2015-09-11 11:38:49', '2015-09-11 11:38:49'),
	(135, 2, 4, '2015-09-11 11:38:49', '2015-09-11 11:38:49'),
	(136, 6, 4, '2015-09-11 11:38:49', '2015-09-11 11:38:49'),
	(137, 5, 4, '2015-09-11 11:38:49', '2015-09-11 11:38:49');
/*!40000 ALTER TABLE `enterprise_payment` ENABLE KEYS */;


-- Copiando estrutura para tabela abastece_com.flag
CREATE TABLE IF NOT EXISTS `flag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `name_anp` varchar(64) DEFAULT NULL COMMENT 'Nome da bandeira pela anp',
  `description` text,
  `img` varchar(32) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela abastece_com.flag: ~7 rows (aproximadamente)
/*!40000 ALTER TABLE `flag` DISABLE KEYS */;
INSERT INTO `flag` (`id`, `name`, `name_anp`, `description`, `img`, `created_at`, `updated_at`) VALUES
	(1, 'Shell', 'Raizen', 'abcdef', 'logotype_914_1.jpg', '2015-09-08 15:10:53', '2015-09-08 15:14:51'),
	(2, 'BR', 'PETROBRAS BR', NULL, 'logotype_862_2.jpg', '2015-09-08 15:15:10', '2015-09-08 15:15:15'),
	(3, 'Ipiranga', 'Ipiranga', NULL, 'logotype_799_3.jpg', '2015-09-08 15:15:38', '2015-09-08 15:15:41'),
	(4, 'Esso', 'Esso', NULL, 'logotype_805_4.jpg', '2015-09-08 15:15:58', '2015-09-08 15:16:02'),
	(5, 'Chevron', 'Chevron', NULL, 'logotype_132_5.jpg', '2015-09-08 15:19:04', '2015-09-08 15:20:38'),
	(6, 'Branca', 'Bandeira Branca', NULL, 'logotype_870_6.jpg', '2015-09-08 15:21:00', '2015-09-08 15:21:03'),
	(7, 'Outras', NULL, NULL, NULL, '2015-09-08 15:22:33', '2015-09-08 15:22:33');
/*!40000 ALTER TABLE `flag` ENABLE KEYS */;


-- Copiando estrutura para tabela abastece_com.log
CREATE TABLE IF NOT EXISTS `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  `type` enum('INSERT','UPDATE','DELETE') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela abastece_com.log: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `log` DISABLE KEYS */;
/*!40000 ALTER TABLE `log` ENABLE KEYS */;


-- Copiando estrutura para tabela abastece_com.message
CREATE TABLE IF NOT EXISTS `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) NOT NULL DEFAULT '0',
  `text` text NOT NULL,
  `enterprise_fk` int(11) NOT NULL,
  `person_fk` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `enteprise_fk4` (`enterprise_fk`),
  KEY `person_fk2` (`person_fk`),
  CONSTRAINT `enteprise_fk4` FOREIGN KEY (`enterprise_fk`) REFERENCES `enterprise` (`id`),
  CONSTRAINT `person_fk2` FOREIGN KEY (`person_fk`) REFERENCES `person` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela abastece_com.message: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
/*!40000 ALTER TABLE `message` ENABLE KEYS */;


-- Copiando estrutura para tabela abastece_com.payment
CREATE TABLE IF NOT EXISTS `payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(16) DEFAULT NULL,
  `description` text,
  `img` varchar(32) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela abastece_com.payment: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `payment` DISABLE KEYS */;
INSERT INTO `payment` (`id`, `name`, `description`, `img`, `created_at`, `updated_at`) VALUES
	(2, 'Dinheiro', NULL, 'logotype_862_2.jpg', '2015-09-04 10:27:02', '2015-09-04 10:47:50'),
	(3, 'Visa', NULL, 'logotype_605_3.jpg', '2015-09-04 10:27:19', '2015-09-04 10:50:11'),
	(4, 'MasterCard', NULL, 'logotype_788_4.jpg', '2015-09-04 10:27:27', '2015-09-04 10:50:16'),
	(5, 'American Express', NULL, 'logotype_530_5.jpg', '2015-09-04 10:27:37', '2015-09-04 10:50:21'),
	(6, 'Ticket Car', NULL, 'logotype_182_6.jpg', '2015-09-04 10:29:57', '2015-09-04 10:50:25');
/*!40000 ALTER TABLE `payment` ENABLE KEYS */;


-- Copiando estrutura para tabela abastece_com.person
CREATE TABLE IF NOT EXISTS `person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `last_name` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `img` varchar(32) NOT NULL,
  `type` enum('ADMIN','EDITOR','USER') NOT NULL,
  `active` tinyint(1) DEFAULT '0',
  `api_token` varchar(40) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela abastece_com.person: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
INSERT INTO `person` (`id`, `name`, `last_name`, `password`, `email`, `img`, `type`, `active`, `api_token`, `created_at`, `updated_at`) VALUES
	(1, 'Tiago', 'Silva Pereira Rodrigues', '$1$/61.aW2.$1UBabAR5PBl.F0zK75TnF0', 'tiagosilvapereira3@gmail.com', 'logotype_547_1.jpg', 'ADMIN', 1, '9d53ad3fa74ccbcec61f54e10788c8f0e3aba7c5', NULL, '2015-09-04 11:42:20'),
	(2, 'Talita', 'Silva Pereira', '$1$Yg..BO4.$4YZkC5s1DpN9lD3uyxku2/', 'Taahsilva95@hotmail.com', '', 'EDITOR', 1, 'b82ca6a7c7df0972b3ce72a56ae665ae5d6012f6', '2015-08-25 17:35:49', '2015-09-03 15:42:21'),
	(4, 'Jéssica', 'Rodrigues Alves Silva', '$1$B51.0e3.$/G3Jrx4pvNcdBaElG/7R5/', 'jessy_alvisccb@hotmail.com', '', 'ADMIN', 1, '1c7ce2b2a40fd4e82e828e24e82a5dbc03f39322', '2015-09-03 16:07:16', '2015-09-03 16:07:16');
/*!40000 ALTER TABLE `person` ENABLE KEYS */;


-- Copiando estrutura para tabela abastece_com.service
CREATE TABLE IF NOT EXISTS `service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `description` text,
  `img` varchar(32) DEFAULT NULL,
  `enterprise_fk` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `enterprise_fk2` (`enterprise_fk`),
  CONSTRAINT `enterprise_fk2` FOREIGN KEY (`enterprise_fk`) REFERENCES `enterprise` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela abastece_com.service: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `service` DISABLE KEYS */;
INSERT INTO `service` (`id`, `name`, `description`, `img`, `enterprise_fk`, `created_at`, `updated_at`) VALUES
	(1, 'Calibragem de Pneu', 'Máquina em ótimo estado de conservação! Faça aqui sua calibragem...', 'service_893_1.jpg', 1, '2015-09-08 16:49:57', '2015-09-08 17:15:17'),
	(2, 'Conveniência e Restaurante Self-Service', 'Temos um ótimo restaurante com valor inicial de 15,00 reais. Venha conferir!!!', 'service_721_2.jpg', 1, '2015-09-08 16:55:49', '2015-09-08 17:21:38'),
	(5, 'Lavagem a seco com Gel Azul', 'Lavamos seu carro bla bla bla...', NULL, 1, '2015-09-08 18:27:40', '2015-09-08 18:27:40'),
	(6, 'Calibragem de Pneu', NULL, 'service_377_6.jpg', 2, '2015-09-10 10:27:26', '2015-09-10 10:27:35');
/*!40000 ALTER TABLE `service` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
