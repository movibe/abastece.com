-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           5.6.12-log - MySQL Community Server (GPL)
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Copiando estrutura do banco de dados para abastece_com
CREATE DATABASE IF NOT EXISTS `abastece_com` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `abastece_com`;


-- Copiando estrutura para tabela abastece_com.comment
CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text NOT NULL,
  `enterprise_fk` int(11) NOT NULL,
  `person_fk` int(11) NOT NULL,
  `person_name` varchar(64) NOT NULL,
  `approved` tinyint(1) NOT NULL,
  `classification` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `enteprise_fk3` (`enterprise_fk`),
  KEY `person_fk` (`person_fk`),
  CONSTRAINT `enteprise_fk3` FOREIGN KEY (`enterprise_fk`) REFERENCES `enterprise` (`id`),
  CONSTRAINT `person_fk` FOREIGN KEY (`person_fk`) REFERENCES `person` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela abastece_com.device
CREATE TABLE IF NOT EXISTS `device` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `notification_token` text,
  `api_token` varchar(40) DEFAULT NULL,
  `last_latitude` varchar(20) DEFAULT NULL,
  `last_longitude` varchar(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela abastece_com.enterprise
CREATE TABLE IF NOT EXISTS `enterprise` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `description` varchar(64) DEFAULT NULL,
  `details` text,
  `flag_fk` int(11) DEFAULT NULL,
  `flag_img` varchar(32) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '0',
  `img` varchar(32) DEFAULT NULL,
  `totalStars` smallint(6) DEFAULT '0' COMMENT 'Total de estrelas da classificação',
  `totalClassified` smallint(6) DEFAULT '0' COMMENT 'Total de pessoas que classificaram',
  `classification` float DEFAULT '0' COMMENT 'Média da classificação',
  `comments` smallint(5) unsigned DEFAULT '0',
  `inscription` varchar(64) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `phone` varchar(16) DEFAULT NULL,
  `url` varchar(128) DEFAULT NULL,
  `credit` double DEFAULT NULL,
  `facebook_url` varchar(128) DEFAULT NULL,
  `street` varchar(128) NOT NULL,
  `number` int(11) DEFAULT NULL,
  `complement` varchar(16) DEFAULT NULL,
  `postal` varchar(16) DEFAULT NULL,
  `district` varchar(32) DEFAULT NULL,
  `country` varchar(32) NOT NULL,
  `state` varchar(32) DEFAULT NULL,
  `city` varchar(64) NOT NULL,
  `latitude` float(10,6) DEFAULT NULL,
  `longitude` float(10,6) DEFAULT NULL,
  `time_initial` varchar(5) DEFAULT NULL,
  `time_final` varchar(5) DEFAULT NULL,
  `hasGasoline` tinyint(1) DEFAULT NULL,
  `hasGasolineLeaded` tinyint(1) DEFAULT NULL,
  `hasGasolinePremium` tinyint(1) DEFAULT NULL,
  `hasEthanol` tinyint(1) DEFAULT NULL,
  `hasDiesel` tinyint(1) DEFAULT NULL,
  `hasDieselS10` tinyint(1) DEFAULT NULL,
  `hasNaturalGas` tinyint(1) DEFAULT NULL,
  `priceGasoline` decimal(10,3) DEFAULT NULL,
  `priceGasolineLeaded` decimal(10,3) DEFAULT NULL,
  `priceGasolinePremium` decimal(10,3) DEFAULT NULL,
  `priceEthanol` decimal(10,3) DEFAULT NULL,
  `priceDiesel` decimal(10,3) DEFAULT NULL,
  `priceDieselS10` decimal(10,3) DEFAULT NULL,
  `priceNaturalGas` decimal(10,3) DEFAULT NULL,
  `fuel_updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `flag_fk` (`flag_fk`),
  KEY `coordinates` (`latitude`,`longitude`),
  CONSTRAINT `flag_fk` FOREIGN KEY (`flag_fk`) REFERENCES `flag` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela abastece_com.enterprise_payment
CREATE TABLE IF NOT EXISTS `enterprise_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_fk` int(11) NOT NULL DEFAULT '0',
  `enterprise_fk` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `payment_fk` (`payment_fk`),
  KEY `enterprise_fk` (`enterprise_fk`),
  CONSTRAINT `enterprise_fk` FOREIGN KEY (`enterprise_fk`) REFERENCES `enterprise` (`id`),
  CONSTRAINT `payment_fk` FOREIGN KEY (`payment_fk`) REFERENCES `payment` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela abastece_com.flag
CREATE TABLE IF NOT EXISTS `flag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `name_anp` varchar(64) DEFAULT NULL COMMENT 'Nome da bandeira pela anp',
  `description` text,
  `img` varchar(32) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela abastece_com.log
CREATE TABLE IF NOT EXISTS `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `table_id` int(11) NOT NULL COMMENT 'O id que foi salvo ou alterado na tabela',
  `table_name` varchar(64) NOT NULL,
  `description` text NOT NULL,
  `person_fk` int(11) NOT NULL,
  `person_name` varchar(64) NOT NULL,
  `type` enum('INSERT','UPDATE','DELETE','LOGIN','LOGOFF') NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `table` (`table_id`,`table_name`),
  KEY `log_person_fk` (`person_fk`),
  CONSTRAINT `log_person_fk` FOREIGN KEY (`person_fk`) REFERENCES `person` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela abastece_com.message
CREATE TABLE IF NOT EXISTS `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) NOT NULL DEFAULT '0',
  `text` text NOT NULL,
  `enterprise_fk` int(11) NOT NULL,
  `person_fk` int(11) NOT NULL,
  `approved` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `enteprise_fk4` (`enterprise_fk`),
  KEY `person_fk2` (`person_fk`),
  CONSTRAINT `enteprise_fk4` FOREIGN KEY (`enterprise_fk`) REFERENCES `enterprise` (`id`),
  CONSTRAINT `person_fk2` FOREIGN KEY (`person_fk`) REFERENCES `person` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela abastece_com.payment
CREATE TABLE IF NOT EXISTS `payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(16) DEFAULT NULL,
  `description` text,
  `img` varchar(32) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela abastece_com.person
CREATE TABLE IF NOT EXISTS `person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `last_name` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `img` varchar(32) NOT NULL,
  `type` enum('ADMIN','EDITOR','EDITOR_JUNIOR','USER') NOT NULL,
  `active` tinyint(1) DEFAULT '0',
  `can_send_email` tinyint(1) DEFAULT '0' COMMENT 'Posso enviar e-mails para esse usuário?',
  `api_token` varchar(40) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela abastece_com.person_enterprise
CREATE TABLE IF NOT EXISTS `person_enterprise` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_fk` int(11) NOT NULL,
  `enterprise_fk` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_pe_enterprise` (`person_fk`,`enterprise_fk`),
  KEY `pe_enterprise_fk` (`enterprise_fk`),
  CONSTRAINT `pe_enterprise_fk` FOREIGN KEY (`enterprise_fk`) REFERENCES `enterprise` (`id`),
  CONSTRAINT `pe_person_fk` FOREIGN KEY (`person_fk`) REFERENCES `person` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela abastece_com.service
CREATE TABLE IF NOT EXISTS `service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `description` text,
  `img` varchar(32) DEFAULT NULL,
  `enterprise_fk` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `enterprise_fk2` (`enterprise_fk`),
  CONSTRAINT `enterprise_fk2` FOREIGN KEY (`enterprise_fk`) REFERENCES `enterprise` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Exportação de dados foi desmarcado.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
