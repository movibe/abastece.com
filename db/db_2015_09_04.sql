-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           5.6.12-log - MySQL Community Server (GPL)
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Copiando estrutura do banco de dados para abastece_com
CREATE DATABASE IF NOT EXISTS `abastece_com` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `abastece_com`;


-- Copiando estrutura para tabela abastece_com.comment
CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text NOT NULL,
  `enterprise_fk` int(11) NOT NULL,
  `person_fk` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `enteprise_fk3` (`enterprise_fk`),
  KEY `person_fk` (`person_fk`),
  CONSTRAINT `enteprise_fk3` FOREIGN KEY (`enterprise_fk`) REFERENCES `enterprise` (`id`),
  CONSTRAINT `person_fk` FOREIGN KEY (`person_fk`) REFERENCES `person` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela abastece_com.comment: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;


-- Copiando estrutura para tabela abastece_com.device
CREATE TABLE IF NOT EXISTS `device` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `notification_token` text,
  `api_token` varchar(40) DEFAULT NULL,
  `last_latitude` varchar(20) DEFAULT NULL,
  `last_longitude` varchar(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela abastece_com.device: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `device` DISABLE KEYS */;
/*!40000 ALTER TABLE `device` ENABLE KEYS */;


-- Copiando estrutura para tabela abastece_com.enterprise
CREATE TABLE IF NOT EXISTS `enterprise` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `description` text,
  `details` text,
  `img` varchar(32) DEFAULT NULL,
  `totalStars` smallint(6) DEFAULT NULL COMMENT 'Total de estrelas da classificação',
  `totalClassified` smallint(6) DEFAULT NULL COMMENT 'Total de pessoas que classificaram',
  `classification` float DEFAULT NULL COMMENT 'Média da classificação',
  `comments` smallint(5) unsigned DEFAULT NULL,
  `inscription` varchar(64) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `phone` varchar(16) DEFAULT NULL,
  `url` varchar(128) DEFAULT NULL,
  `credit` double DEFAULT NULL,
  `facebook_url` varchar(128) DEFAULT NULL,
  `street` varchar(128) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `complement` varchar(16) DEFAULT NULL,
  `postal` varchar(16) DEFAULT NULL,
  `country` varchar(32) DEFAULT NULL,
  `state` varchar(32) DEFAULT NULL,
  `city` varchar(64) DEFAULT NULL,
  `latitude` float(13,10) DEFAULT NULL,
  `longitude` float(13,10) DEFAULT NULL,
  `time_initial` varchar(5) DEFAULT NULL,
  `time_final` varchar(5) DEFAULT NULL,
  `hasGasoline` tinyint(1) DEFAULT NULL,
  `hasGasolineLeaded` tinyint(1) DEFAULT NULL,
  `hasGasolinePremium` tinyint(1) DEFAULT NULL,
  `hasEthanol` tinyint(1) DEFAULT NULL,
  `hasDiesel` tinyint(1) DEFAULT NULL,
  `hasDieselS10` tinyint(1) DEFAULT NULL,
  `hasNaturalGas` tinyint(1) DEFAULT NULL,
  `priceGasoline` double DEFAULT NULL,
  `priceGasolineLeaded` double DEFAULT NULL,
  `priceGasolinePremium` double DEFAULT NULL,
  `priceEthanol` double DEFAULT NULL,
  `priceDiesel` double DEFAULT NULL,
  `priceDieselS10` double DEFAULT NULL,
  `priceNaturalGas` double DEFAULT NULL,
  `fuel_updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela abastece_com.enterprise: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `enterprise` DISABLE KEYS */;
INSERT INTO `enterprise` (`id`, `name`, `description`, `details`, `img`, `totalStars`, `totalClassified`, `classification`, `comments`, `inscription`, `email`, `phone`, `url`, `credit`, `facebook_url`, `street`, `number`, `complement`, `postal`, `country`, `state`, `city`, `latitude`, `longitude`, `time_initial`, `time_final`, `hasGasoline`, `hasGasolineLeaded`, `hasGasolinePremium`, `hasEthanol`, `hasDiesel`, `hasDieselS10`, `hasNaturalGas`, `priceGasoline`, `priceGasolineLeaded`, `priceGasolinePremium`, `priceEthanol`, `priceDiesel`, `priceDieselS10`, `priceNaturalGas`, `fuel_updated_at`, `created_at`, `updated_at`) VALUES
	(1, 'Teste', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-09-04 12:08:21', '2015-09-04 12:08:21');
/*!40000 ALTER TABLE `enterprise` ENABLE KEYS */;


-- Copiando estrutura para tabela abastece_com.enterprise_payment
CREATE TABLE IF NOT EXISTS `enterprise_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_fk` int(11) NOT NULL DEFAULT '0',
  `enterprise_fk` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `payment_fk` (`payment_fk`),
  KEY `enterprise_fk` (`enterprise_fk`),
  CONSTRAINT `enterprise_fk` FOREIGN KEY (`enterprise_fk`) REFERENCES `enterprise` (`id`),
  CONSTRAINT `payment_fk` FOREIGN KEY (`payment_fk`) REFERENCES `payment` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela abastece_com.enterprise_payment: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `enterprise_payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `enterprise_payment` ENABLE KEYS */;


-- Copiando estrutura para tabela abastece_com.log
CREATE TABLE IF NOT EXISTS `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  `type` enum('INSERT','UPDATE','DELETE') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela abastece_com.log: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `log` DISABLE KEYS */;
/*!40000 ALTER TABLE `log` ENABLE KEYS */;


-- Copiando estrutura para tabela abastece_com.message
CREATE TABLE IF NOT EXISTS `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) NOT NULL DEFAULT '0',
  `text` text NOT NULL,
  `enterprise_fk` int(11) NOT NULL,
  `person_fk` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `enteprise_fk4` (`enterprise_fk`),
  KEY `person_fk2` (`person_fk`),
  CONSTRAINT `enteprise_fk4` FOREIGN KEY (`enterprise_fk`) REFERENCES `enterprise` (`id`),
  CONSTRAINT `person_fk2` FOREIGN KEY (`person_fk`) REFERENCES `person` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela abastece_com.message: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
/*!40000 ALTER TABLE `message` ENABLE KEYS */;


-- Copiando estrutura para tabela abastece_com.payment
CREATE TABLE IF NOT EXISTS `payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(16) DEFAULT NULL,
  `description` text,
  `img` varchar(32) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela abastece_com.payment: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `payment` DISABLE KEYS */;
INSERT INTO `payment` (`id`, `name`, `description`, `img`, `created_at`, `updated_at`) VALUES
	(2, 'Dinheiro', NULL, 'logotype_862_2.jpg', '2015-09-04 10:27:02', '2015-09-04 10:47:50'),
	(3, 'Visa', NULL, 'logotype_605_3.jpg', '2015-09-04 10:27:19', '2015-09-04 10:50:11'),
	(4, 'MasterCard', NULL, 'logotype_788_4.jpg', '2015-09-04 10:27:27', '2015-09-04 10:50:16'),
	(5, 'American Express', NULL, 'logotype_530_5.jpg', '2015-09-04 10:27:37', '2015-09-04 10:50:21'),
	(6, 'Ticket Car', NULL, 'logotype_182_6.jpg', '2015-09-04 10:29:57', '2015-09-04 10:50:25');
/*!40000 ALTER TABLE `payment` ENABLE KEYS */;


-- Copiando estrutura para tabela abastece_com.person
CREATE TABLE IF NOT EXISTS `person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `last_name` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `img` varchar(32) NOT NULL,
  `type` enum('ADMIN','EDITOR','USER') NOT NULL,
  `active` tinyint(1) DEFAULT '0',
  `api_token` varchar(40) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela abastece_com.person: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
INSERT INTO `person` (`id`, `name`, `last_name`, `password`, `email`, `img`, `type`, `active`, `api_token`, `created_at`, `updated_at`) VALUES
	(1, 'Tiago', 'Silva Pereira Rodrigues', '$1$/61.aW2.$1UBabAR5PBl.F0zK75TnF0', 'tiagosilvapereira3@gmail.com', 'logotype_547_1.jpg', 'ADMIN', 1, '9d53ad3fa74ccbcec61f54e10788c8f0e3aba7c5', NULL, '2015-09-04 11:42:20'),
	(2, 'Talita', 'Silva Pereira', '$1$Yg..BO4.$4YZkC5s1DpN9lD3uyxku2/', 'Taahsilva95@hotmail.com', '', 'EDITOR', 1, 'b82ca6a7c7df0972b3ce72a56ae665ae5d6012f6', '2015-08-25 17:35:49', '2015-09-03 15:42:21'),
	(4, 'Jéssica', 'Rodrigues Alves Silva', '$1$B51.0e3.$/G3Jrx4pvNcdBaElG/7R5/', 'jessy_alvisccb@hotmail.com', '', 'ADMIN', 1, '1c7ce2b2a40fd4e82e828e24e82a5dbc03f39322', '2015-09-03 16:07:16', '2015-09-03 16:07:16');
/*!40000 ALTER TABLE `person` ENABLE KEYS */;


-- Copiando estrutura para tabela abastece_com.services
CREATE TABLE IF NOT EXISTS `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(16) DEFAULT NULL,
  `description` text,
  `img` varchar(32) DEFAULT NULL,
  `enterprise_fk` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `enterprise_fk2` (`enterprise_fk`),
  CONSTRAINT `enterprise_fk2` FOREIGN KEY (`enterprise_fk`) REFERENCES `enterprise` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela abastece_com.services: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
