angular.module('admin-services', [])

.factory('Users', function($rootScope, $http, Requisition){

	return {

		all : function(max, offset, desc){
			
			return Requisition.all(max, offset, desc);

		},

		get : function(id){

			return Requisition.get(id);

		},

		save : function(data){

			return Requisition.save(data);
			

		},

		delete : function(id){

			return Requisition.delete(id);

		},

		login : function(email, password){
			
			return $http.post(Requisition.getFullURL('user') + '/login/', 
			{'email' : email, 'password' : password});

		},

		linkOrUnlink : function(user, enterprise, mode){
			
			return $http.post(Requisition.getFullURL('user') + '/' + user + '/' + mode + '/' + enterprise);

		},

	}

})

.factory('Enterprises', function($rootScope, $http, Requisition){

	return {

		all : function(max, offset, desc){
			
			return Requisition.all(max, offset, desc);

		},

		get : function(id){

			return Requisition.get(id);

		},

		save : function(data){

			return Requisition.save(data);	

		},

		delete : function(id){

			return Requisition.delete(id);

		},

		saveService : function(data, enterprise_id){

			if(data.id){

				return $http.put(Requisition.getFullURL('enterprise') + '/service/' + data.id, data);

			}else{

				return $http.post(Requisition.getFullURL('enterprise') + '/' + enterprise_id + '/services', data);

			}	

		},

		deleteService : function(id){

			return $http.delete(Requisition.getFullURL('enterprise') + '/service/' + id);

		},

		getEnterpriseServices : function(enterprise_id){

			return $http.get(Requisition.getFullURL('enterprise') + '/' + enterprise_id + '/services');

		}

	}

})

.factory('Payments', function($rootScope, $http, Requisition){

	return {

		all : function(max, offset, desc){
			
			return Requisition.all(max, offset, desc);

		},

		get : function(id){

			return Requisition.get(id);

		},

		save : function(data){
			
			return Requisition.save(data);
			
		},

		delete : function(id){

			return Requisition.delete(id);

		}

	}

})

.factory('Flags', function($rootScope, $http, Requisition){

	return {

		all : function(max, offset, desc){
			
			return Requisition.all(max, offset, desc);

		},

		get : function(id){

			return Requisition.get(id);

		},

		save : function(data){
			
			return Requisition.save(data);
			
		},

		delete : function(id){

			return Requisition.delete(id);

		}

	}

})