angular.module('admin', ['ngRoute', 'ngResource', 'ngFileUpload', 'admin.controllers',
			   			 'admin-services', 'pagination', 'requisition', 'ngMask'])

.run(function($rootScope, Pagination, Requisition, $templateCache){

	// Limpa o cache sempre que carrega uma View
	$rootScope.$on('$viewContentLoaded', function() {
      $templateCache.removeAll();
   	});

	Requisition.setURL('http://localhost:8080/abastece_com/api/');

	//Paginação
	Pagination.setPageLimit(10);
	Pagination.resetPagination();

	//Qual elemento está ativo para uso do menu
	$rootScope.menuActive = '';

	// Configurações do TOAST
	toastr.options = {
	  "closeButton": true,
	  "debug": false,
	  "newestOnTop": false,
	  "progressBar": false,
	  "positionClass": "toast-top-right",
	  "preventDuplicates": false,
	  "onclick": null,
	  "showDuration": "300",
	  "hideDuration": "500",
	  "timeOut": "2500",
	  "extendedTimeOut": "500",
	  "showEasing": "swing",
	  "hideEasing": "linear",
	  "showMethod": "fadeIn",
	  "hideMethod": "fadeOut"
	}

})

.config(function($routeProvider){

	$routeProvider
	.when('/', {
		resolve: {
			'check' : function($rootScope, $location){

				if(!$rootScope.isLogged){
					$location.path('/login');
				}

			 }
		},
		templateUrl : 'pages/home.html',
		controller : 'HomeController'
	})
	.when('/login', {
		templateUrl : 'login.html',
		controller : 'LoginController'
	})
	.when('/enterprises', {
		resolve: {
			'check' : function($rootScope, $location){

				if(!$rootScope.isLogged){
					$location.path('/login');
				}

			 }
		},
		templateUrl : 'pages/enterprises.html',
		controller : 'EnterprisesController'
	})
	.when('/enterprise/:id?', {
		resolve: {
			'check' : function($rootScope, $location){

				if(!$rootScope.isLogged){
					$location.path('/login');
				}

			 }
		},
		templateUrl : 'pages/enterprise.html',
		controller : 'EnterpriseController'
	})
	.when('/users', {
		resolve: {
			'check' : function($rootScope, $location){

				if(!$rootScope.isLogged){
					$location.path('/login');
				}

			 }
		},
		templateUrl : 'pages/users.html',
		controller : 'UsersController'
	})
	.when('/user/:id?', {
		resolve: {
			'check' : function($rootScope, $location){

				if(!$rootScope.isLogged){
					$location.path('/login');
				}

			 }
		},
		templateUrl : 'pages/user.html',
		controller : 'UserController'
	})
	.when('/payments', {
		resolve: {
			'check' : function($rootScope, $location){

				if(!$rootScope.isLogged){
					$location.path('/login');
				}

			 }
		},
		templateUrl : 'pages/payments.html',
		controller : 'PaymentsController'
	})
	.when('/payment/:id?', {
		resolve: {
			'check' : function($rootScope, $location){

				if(!$rootScope.isLogged){
					$location.path('/login');
				}

			 }
		},
		templateUrl : 'pages/payment.html',
		controller : 'PaymentController'
	})
	.when('/flags', {
		resolve: {
			'check' : function($rootScope, $location){

				if(!$rootScope.isLogged){
					$location.path('/login');
				}

			 }
		},
		templateUrl : 'pages/flags.html',
		controller : 'FlagsController'
	})
	.when('/flag/:id?', {
		resolve: {
			'check' : function($rootScope, $location){

				if(!$rootScope.isLogged){
					$location.path('/login');
				}

			 }
		},
		templateUrl : 'pages/flag.html',
		controller : 'FlagController'
	})
	.otherwise({
        redirectTo: '/'
    });

	//$locationProvider.html5Mode(true);

})