// Utilidades em JavaScript

// Função que verifica se uma hora é maior do que outra
// Formato da hora 00:00
// Se for passado no formato 00:00:00 considerará apenas a hora e os minutos
function timeIsGreater(hour1, hour2){

	total_minutes_1 = timeToMinutes(hour1);
	total_minutes_2 = timeToMinutes(hour2);
	
	return (total_minutes_1 > total_minutes_2);

}

// Converte uma hora em minutos
function timeToMinutes(time){ 

	var h1 = parseInt(time.substr(0,2), 10); //Horas
	var m1 = parseInt(time.substr(3,2), 10); //Minutos
	
	//alert(h1 + ' - ' + m1);

	total_minutes_1 = (h1 * 60) + m1;

	//alert(total_minutes_1);

	return (total_minutes_1);

}

function minutesToTime(minutes){

	var h1 = parseInt((minutes / 60), 10);
	var m1 = parseInt((minutes % 60), 10);

	// Coloca o 0 à esquerda se necessário
	h1 = h1 < 10 ? '0' + h1 : h1;
	m1 = m1 < 10 ? '0' + m1 : m1;

	return(h1 + ':' + m1);

}

function myLocaleDate(date){

	var day   = parseInt(date.getDate(), 10);
	var month = parseInt(date.getMonth() + 1, 10);
	var year  = parseInt(date.getFullYear(), 10);

	// Coloca o 0 à esquerda se necessário
	day = day < 10 ? '0' + day : day;
	month = month < 10 ? '0' + month : month;

	return(day + '/' + month + '/' + year);

}

// Separa os elementos de uma data DD/MM/YYYY
function separateDate(date){

	var my_day  = date.substr(0,2);
	var my_mon  = date.substr(3,2);
	var my_year = date.substr(6,4);

	return {day : my_day, month : my_mon, year : my_year};

}

// Separa os elementos de uma data do tipo Banco de Dados YYYY-MM-DD
function separateDateDB(date){

	var my_day  = date.substr(6,2);
	var my_mon  = date.substr(5,2);
	var my_year = date.substr(0,4);

	return {day : my_day, month : my_mon, year : my_year};

}

// Retorna o dia da semana de uma data do tipo Banco de Dados YYYY-MM-DD
function dbWeekDay(date){

	var my_date = date.split('-');

	// Cria uma nova data
	var d = new Date(my_date[0], my_date[1] - 1, my_date[2]);

	console.log(d);

	// Obtém o dia da semana pela data
	return d.getDay();

}

// Gera a ISO String de uma data do input com máscara
function inputToISO(date){

	var separate = date.split(' ');

	var my_date  = separate[0].split('/');

	return (my_date[2] + '-' + my_date[1] + '-' + my_date[0] + 'T' + separate[1] + ':00.000Z');

}

// Obtém o Timestamp mysql e retorna no formato do input
function mysqlTimeToInput(date){

	//2012-12-12 18:00:00
	var separate = date.split(' ');

	var my_date  = separate[0].split('-');
	var my_time  = separate[1].split(':');

	return (my_date[2] + '/' + my_date[1] + '/' + my_date[0] + ' ' + my_time[0] + ':' + my_time[1]);

}