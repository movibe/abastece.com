angular.module('admin.controllers')

.controller('FlagsController', function($scope, $rootScope, $http, $location, Flags, Pagination, Requisition){

	$scope.loading = true;
	$rootScope.menuActive = 'flags';
	var page_limit = Pagination.getPageLimit();
	Requisition.setModel('flag');

	$scope.allFlags = function(max, offset, description){

		$scope.loading = true;

		flags = Flags.all(max, offset, description);
		flags.then(function(success){

			$scope.loading = false;
			
			if(!success.data.error){
				Pagination.setPages(success.data.table_count);
				$scope.flags = success.data.data;
				$scope.flags.map(function(el){
					el.name = (el.name_anp) ? el.name + ' - ' + el.name_anp : el.name;
				})
			}else{
				toastr.error('Erro ao buscar o usuário!')
			}

			
		
		}, function(error){

			$scope.loading = false;
			toastr.error('Erro na requisição! Verifique a conexão com a internet.');
		});

	}

	Pagination.resetPagination();
	$scope.allFlags(Pagination.getPageLimit(), 0);

	$scope.search = function(max, offset, description){

		Pagination.resetPagination();
		$scope.allFlags(max, offset, description);

	}

	$scope.goToPage = function(mode){

		switch(mode){
			case 'next':
				var offset = Pagination.nextPage();
				$scope.allFlags(page_limit, offset);
				break;
			case 'previous':
				var offset = Pagination.previousPage();
				$scope.allFlags(page_limit, offset);
				break;
			case 'first':
				var offset = Pagination.firstPage();
				$scope.allFlags(page_limit, offset);
				break;
			case 'last':
				var offset = Pagination.lastPage();
				$scope.allFlags(page_limit, offset);
				break;
		}

	}

	$scope.new = function(){

		$location.path('/flag');

	}

	$scope.edit = function(id){
		$location.path('/flag/' + id);
	}

	$scope.delete = function(id){
		
		if(confirm('Tem certeza que deseja deletar esse registro?')){
			Flags.delete(id).then(
			function(success){

				if(!success.data.error){
					offset = Pagination.refreshPage();
					$scope.allFlags(page_limit, offset);
				}else{
					console.log(success.data.message)
					toastr.warning(success.data.message)
				}
				
			},
			function(error){
				toastr.error('Erro na requisição! Verifique a conexão com a internet.');
			})
		}

	}

})

.controller('FlagController', function($scope, $rootScope, Flags, $location, $routeParams, Requisition, Upload){

	$rootScope.menuActive = 'flags';
	Requisition.setModel('flag');

	$scope.$watch('files', function () {
        $scope.upload($scope.files);
    });

    $scope.upload = function (files) {
        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                Upload.upload({
                    url: Requisition.getFullURL('flag') + '/' + $routeParams.id + '/image/upload',
                    fields: {},
                    file: file
                }).progress(function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
                }).success(function (data, status, headers, config) {
                    console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
                });
            }
        }
    };

	// Se recebeu um id, carrega o usuário
	if($routeParams.id){

		Flags.get($routeParams.id).then(
		function(success){

			if(!success.data.error){
				$scope.flag = success.data.data;
			}else{
				console.log(success.data.message)
			}

		},
		function(error){
			toastr.error('Erro na requisição! Verifique a conexão com a internet.');
		})

	}

	$scope.save = function(flag){

		$scope.saving = true;

		save = Flags.save(flag);

		save.then(function(success){

			if(!success.data.error){
				toastr.success('Salvo com sucesso!')
				$location.path('/flag/' + success.data.data.id);
			}else{
				console.log(success.data.message);
				toastr.warning(success.data.message)
			}

			$scope.saving = false;

		}, function(error){

			$scope.saving = false;
			toastr.error('Erro na requisição! Verifique a conexão com a internet.')

		})

	}

	$scope.sendForm = function(flag){
		
		// Verifica se possui ID, para salvar ou criar um novo registro
		$scope.save(flag);

	}

})