angular.module('admin.controllers')

.controller('EnterprisesController', function($scope, $rootScope, $http, $location, Enterprises, Pagination, Requisition){

	$scope.loading = true;
	$rootScope.menuActive = 'enterprises';
	var page_limit = Pagination.getPageLimit();
	Requisition.setModel('enterprise');

	$scope.allEnterprises = function(max, offset, description){

		$scope.loading = true;

		enterprises = Enterprises.all(max, offset, description);
		enterprises.then(function(success){

			$scope.loading = false;
			
			if(!success.data.error){
				Pagination.setPages(success.data.table_count);
				$scope.enterprises = success.data.data;

				// Realiza o mapping do endereço
				$scope.enterprises.map(function(el){
					el.address = el.street + ', ' + el.number + ', ' + el.district + ' - ' + el.city;
				})

			}else{
				toastr.error('Erro ao buscar os segmentos!')
			}

			
		
		}, function(error){

			$scope.loading = false;
			toastr.error('Erro na requisição! Verifique a conexão com a internet.');
		});

	}

	Pagination.resetPagination();
	$scope.allEnterprises(Pagination.getPageLimit(), 0);

	$scope.search = function(max, offset, description){

		Pagination.resetPagination();
		$scope.allEnterprises(max, offset, description);

	}

	$scope.goToPage = function(mode){

		switch(mode){
			case 'next':
				var offset = Pagination.nextPage();
				$scope.allEnterprises(page_limit, offset);
				break;
			case 'previous':
				var offset = Pagination.previousPage();
				$scope.allEnterprises(page_limit, offset);
				break;
			case 'first':
				var offset = Pagination.firstPage();
				$scope.allEnterprises(page_limit, offset);
				break;
			case 'last':
				var offset = Pagination.lastPage();
				$scope.allEnterprises(page_limit, offset);
				break;
		}

	}

	$scope.new = function(){

		$location.path('/enterprise');

	}

	$scope.edit = function(id){
		$location.path('/enterprise/' + id);
	}

	$scope.delete = function(id){
		
		if(confirm('Tem certeza que deseja deletar esse registro?')){
			Enterprises.delete(id).then(
			function(success){

				if(!success.data.error){
					offset = Pagination.refreshPage();
					$scope.allEnterprises(page_limit, offset);
				}else{
					console.log(success.data.message)
					toastr.warning(success.data.message)
				}
				
			},
			function(error){
				toastr.error('Erro na requisição! Verifique a conexão com a internet.');
			})
		}

	}
	
})

.controller('EnterpriseController', function($scope, $rootScope, Enterprises, Payments, Flags, $location, $routeParams,
 											 $http, Requisition, Upload){

	$scope.enterprise = {payments : {}, sel_payments : []};

	$scope.$watch('files', function () {
		var url = Requisition.getFullURL('enterprise') + '/' + $routeParams.id + '/image/upload'; 
        $scope.upload($scope.files, url);
    });

    $scope.$watch('files2', function () {
    	console.log($scope.service);
    	// Envia a imagem do serviço que está carregado no escopo
    	var url = Requisition.getFullURL('enterprise') + '/service/' + $scope.service.id + '/image/upload';
        $scope.upload($scope.files2, url);
    });

    $scope.upload = function (files, _url) {
        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                Upload.upload({
                    url: _url,
                    fields: {},
                    file: file
                }).progress(function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
                }).success(function (data, status, headers, config) {
                    console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
                });
            }
        }
    };
	
	$rootScope.menuActive = 'enterprises';
	Requisition.setModel('enterprise');

	// Se recebeu um id, carrega o usuário
	if($routeParams.id){

		Enterprises.get($routeParams.id).then(
		function(success){

			if(!success.data.error){
				
				$scope.enterprise 						= success.data.data;
				$scope.enterprise.hasGasoline 			= ($scope.enterprise.hasGasoline == 1 || $scope.enterprise.hasGasoline == true) ? true : false;
				$scope.enterprise.hasGasolineLeaded 	= ($scope.enterprise.hasGasolineLeaded == 1 || $scope.enterprise.hasGasolineLeaded == true) ? true : false;
				$scope.enterprise.hasGasolinePremium 	= ($scope.enterprise.hasGasolinePremium == 1 || $scope.enterprise.hasGasolinePremium == true) ? true : false;
				$scope.enterprise.hasEthanol	 		= ($scope.enterprise.hasEthanol == 1 || $scope.enterprise.hasEthanol == true) ? true : false;
				$scope.enterprise.hasDiesel 			= ($scope.enterprise.hasDiesel == 1 || $scope.enterprise.hasDiesel == true) ? true : false;
				$scope.enterprise.hasDieselS10 			= ($scope.enterprise.hasDieselS10 == 1 || $scope.enterprise.hasDieselS10 == true) ? true : false;
				$scope.enterprise.hasNaturalGas 		= ($scope.enterprise.hasNaturalGas == 1 || $scope.enterprise.hasNaturalGas == true) ? true : false;
				$scope.enterprise.active = ($scope.enterprise.active == 1 || $scope.enterprise.active == true) ? true : false;
				// Obtém os serviços
				$scope.getEnterpriseServices();

				// A seleção inicial com as formas de pagamento da empresa
				$scope.selection = $scope.enterprise.payments;

				// Obtém as formas de pagamento
				$scope.getPaymentsList();

			}else{
				console.log(success.data.message)
			}

		},
		function(error){
			toastr.error('Erro na requisição! Verifique a conexão com a internet.');
		})

	}

	$scope.save = function(enterprise){

		enterprise.selection = $scope.selection;

		Enterprises.save(enterprise)
		.then(function(success){

			if(!success.data.error){
				toastr.success('Salvo com sucesso!')
				$location.path('/enterprise/' + success.data.data.id);
			}else{
				console.log(success.data.message);
				toastr.error(success.data.message)
			}

		}, function(error){

			toastr.error('Erro na requisição! Verifique a conexão com a internet.')

		})

	}

	$scope.sendForm = function(enterprise){

		$scope.save(enterprise);

	}

	$scope.getCoordinates = function(address){

		if(address){

			street = address.street.replace(' ', '+');

			url = 'https://maps.google.com/maps/api/geocode/json?address='+address.number+'+'
					+street+'+'+address.city+'+'+address.country+'&sensor=false';

			$http({
			    method: 'GET',
			    url: url,
			    // Aqui deletamos os headers para utilizar essa requisição externa
			    transformRequest: function(data, headersGetter) {
			        var headers = headersGetter();

			        delete headers['ident'];
			        delete headers['token'];
			        delete headers['type'];

			        return headers;
			    }
			}).then(
			function(success){
				
				data = success.data;

				if(data.results.length > 0){
					loc = data.results[0].geometry.location;

					$scope.enterprise.latitude  = loc.lat;
					$scope.enterprise.longitude = loc.lng;

					toastr.info('Coordenadas encontradas ' + address.street + ', ' + address.number)
				
				}else{

					$scope.enterprise.latitude  = 0;
					$scope.enterprise.longitude = 0;

					toastr.info('Não existem coordenadas para ' + address.street + ', ' + address.number)

				}
				
			},
			function(error){
				toastr.error('Erro na requisição! Verifique a conexão com a internet.')
			})

		}

	}

	$scope.getPaymentsList = function(){

		Requisition.setModel('payment');
		Payments.all().
		then(function(success){

			$scope.payments_list = success.data.data;
			Requisition.setModel('enterprise');

		}, function(error){
			console.log('Erro ao carregar os endereços');
			Requisition.setModel('enterprise');
		})

	}

	$scope.allFlags = function(){

		// Seta que vai trabalhar com o model de flags
		Requisition.setModel('flag');
		
		Flags.all()
		.then(function(success){

			if(!success.data.error){
				
				$scope.flags = success.data.data;
				$scope.flags.map(function(el){
					el.name = (el.name_anp) ? el.name + ' - ' + el.name_anp : el.name;
				})

			}else{
				toastr.error('Erro ao buscar as bandeiras!')
			}
		
		}, function(error){

			toastr.error('Erro na requisição! Verifique a conexão com a internet.');

		});

		// Retorna para o model da promoção
		Requisition.setModel('enterprise');

	}

	// Obtém as bandeiras 
	$scope.allFlags();

	$scope.getEnterpriseServices = function(){

		Enterprises.getEnterpriseServices($routeParams.id).
		then(function(success){

			$scope.services = success.data.data;

		}, function(error){
			console.log('Erro ao carregar os serviços');
		})

	}

	$scope.sendService = function(service){

		Enterprises.saveService(service, $routeParams.id)
		.then(function(success){

			$scope.service = {};
			$scope.getEnterpriseServices();
			toastr.info('Serviço salvo com sucesso!')

		}, function(error){

			toastr.error('Erro na requisição! Verifique a conexão com a internet.')

		})

	}

	$scope.editService = function(service){

		$scope.service = service;

	}

	$scope.deleteService = function(id){

		if(confirm("Deseja excluir este serviço?")){
		
			Enterprises.deleteService(id)
			.then(function(success){
	
				$scope.getEnterpriseServices();
	
			}, function(error){
	
				toastr.error('Erro na requisição! Verifique a conexão com a internet.')
	
			})
		}
	}

	// Trabalhando com o checkbox
	$scope.selection = [];

	$scope.isChecked = function(id){
      var match = false;
      for(var i=0 ; i < $scope.selection.length; i++) {
        if($scope.selection[i].id == id){
          match = true;
        }
      }
      return match;
  	};

  	$scope.sync = function(bool, item){
	    if(bool){
	      // Adiciona o item
	      $scope.selection.push(item);
	    } else {
	      // Remove o item
	      for(var i=0 ; i < $scope.selection.length; i++) {
	        if($scope.selection[i].id == item.id){
	          $scope.selection.splice(i,1);
	        }
	      }      
	    }

	  };

})
