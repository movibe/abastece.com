angular.module('admin.controllers')

.controller('PaymentsController', function($scope, $rootScope, $http, $location, Payments, Pagination, Requisition){

	$scope.loading = true;
	$rootScope.menuActive = 'payments';
	var page_limit = Pagination.getPageLimit();
	Requisition.setModel('payment');

	$scope.allPayments = function(max, offset, description){

		$scope.loading = true;

		payments = Payments.all(max, offset, description);
		payments.then(function(success){

			$scope.loading = false;
			
			if(!success.data.error){
				Pagination.setPages(success.data.table_count);
				$scope.payments = success.data.data;
			}else{
				toastr.error('Erro ao buscar o usuário!')
			}

			
		
		}, function(error){

			$scope.loading = false;
			toastr.error('Erro na requisição! Verifique a conexão com a internet.');
		});

	}

	Pagination.resetPagination();
	$scope.allPayments(Pagination.getPageLimit(), 0);

	$scope.search = function(max, offset, description){

		Pagination.resetPagination();
		$scope.allPayments(max, offset, description);

	}

	$scope.goToPage = function(mode){

		switch(mode){
			case 'next':
				var offset = Pagination.nextPage();
				$scope.allPayments(page_limit, offset);
				break;
			case 'previous':
				var offset = Pagination.previousPage();
				$scope.allPayments(page_limit, offset);
				break;
			case 'first':
				var offset = Pagination.firstPage();
				$scope.allPayments(page_limit, offset);
				break;
			case 'last':
				var offset = Pagination.lastPage();
				$scope.allPayments(page_limit, offset);
				break;
		}

	}

	$scope.new = function(){

		$location.path('/payment');

	}

	$scope.edit = function(id){
		$location.path('/payment/' + id);
	}

	$scope.delete = function(id){
		
		if(confirm('Tem certeza que deseja deletar esse registro?')){
			Payments.delete(id).then(
			function(success){

				if(!success.data.error){
					offset = Pagination.refreshPage();
					$scope.allPayments(page_limit, offset);
				}else{
					console.log(success.data.message)
					toastr.warning(success.data.message)
				}
				
			},
			function(error){
				toastr.error('Erro na requisição! Verifique a conexão com a internet.');
			})
		}

	}

})

.controller('PaymentController', function($scope, $rootScope, Payments, $location, $routeParams, Requisition, Upload){

	$rootScope.menuActive = 'payments';
	Requisition.setModel('payment');

	$scope.$watch('files', function () {
        $scope.upload($scope.files);
    });

    $scope.upload = function (files) {
        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                Upload.upload({
                    url: Requisition.getFullURL('payment') + '/' + $routeParams.id + '/image/upload',
                    fields: {},
                    file: file
                }).progress(function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
                }).success(function (data, status, headers, config) {
                    console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
                });
            }
        }
    };

	// Se recebeu um id, carrega o usuário
	if($routeParams.id){

		Payments.get($routeParams.id).then(
		function(success){

			if(!success.data.error){
				$scope.payment = success.data.data;
			}else{
				console.log(success.data.message)
			}

		},
		function(error){
			toastr.error('Erro na requisição! Verifique a conexão com a internet.');
		})

	}

	$scope.save = function(payment){

		$scope.saving = true;

		save = Payments.save(payment);

		save.then(function(success){

			if(!success.data.error){
				toastr.success('Salvo com sucesso!')
				$location.path('/payment/' + success.data.data.id);
			}else{
				console.log(success.data.message);
				toastr.warning(success.data.message)
			}

			$scope.saving = false;

		}, function(error){

			$scope.saving = false;
			toastr.error('Erro na requisição! Verifique a conexão com a internet.')

		})

	}

	$scope.sendForm = function(payment){
		
		// Verifica se possui ID, para salvar ou criar um novo registro
		$scope.save(payment);

	}

})