<?php

// Seta o timezone padrão
date_default_timezone_set('America/Sao_Paulo');

// Define o diretório dos arquivos da aplicação
define("_APP", dirname(__FILE__) . '/app');

// Autoloader do composer, para carregar as classes necessárias
require 'vendor/autoload.php';


$app = new \Slim\Slim(array(
  'debug' => true
));
$app->response()->header('Content-Type', 'application/json;charset=utf-8');

require_once _APP . '/config/database.php';
require_once _APP . "/models/appModels.php";
require_once _APP . '/helpers/appHelpers.php';

$authentication = function(){

	$app = \Slim\Slim::getInstance();
	$id = $app->request->headers->get('HTTP_IDENT');
	$token = $app->request->headers->get('HTTP_TOKEN');
	$type = $app->request->headers->get('HTTP_TYPE');

	// Valida o acesso
	if($type == 'USER'){
		$validAccess = Users::where('id','=',$id)->where('api_token','=',$token)->get();
	}else
	if($type == 'DEVICE'){
		$validAccess = Devices::where('id','=',$id)->where('api_token','=',$token)->get();
	}
	
	try{

		if(empty($validAccess) || count($validAccess) <= 0){
			throw new Exception("You don't have permission to access!");
		}

	}catch(Exception $e){
		$app->status(401);
		echo json_encode(array('status' => 'ERROR', 'msg' => $e->getMessage() ));
		$app->stop();
	}

};

// Controllers
require_once _APP . "/controllers/usersControllers.php";
require_once _APP . "/controllers/paymentsControllers.php";
require_once _APP . "/controllers/enterprisesControllers.php";
require_once _APP . "/controllers/flagsControllers.php";

// Constantes
define('UPLOAD_DIR', '../mydash/upload');

# Raiz
$app->get('/', $authentication, function () {
	echo json_encode("API - You can access :D");
});

$app->run();