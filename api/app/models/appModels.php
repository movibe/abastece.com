<?php

class Users extends Illuminate\Database\Eloquent\Model
{
  protected $table = 'person';
}

class Users_Enterprises extends Illuminate\Database\Eloquent\Model
{
  protected $table = 'person_enterprise';
}


class Logs extends Illuminate\Database\Eloquent\Model
{
  protected $table = 'log';
}

class Payments extends Illuminate\Database\Eloquent\Model
{
  protected $table = 'payment';
}

class Devices extends Illuminate\Database\Eloquent\Model
{
  protected $table = 'device';
}

class Enterprises extends Illuminate\Database\Eloquent\Model
{
  protected $table = 'enterprise';
}

class Enterprises_Payments extends Illuminate\Database\Eloquent\Model
{
  protected $table = 'enterprise_payment';
}

class Flags extends Illuminate\Database\Eloquent\Model
{
  protected $table = 'flag';
}

class Services extends Illuminate\Database\Eloquent\Model
{
  protected $table = 'service';
}

class Comments extends Illuminate\Database\Eloquent\Model
{
  protected $table = 'comment';
}