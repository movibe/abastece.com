<?php

# Funções/Controllers da API

# VERSÃO 1 - V1

$app->get('/v1/users', $authentication, function() use ($app) {

  $results = [];
  $description = $app->request->get('description');
  $description = (!empty($description)) ? $description : '';
  
  // Limite e Registros que serão 'pulados'
  $limit = $app->request->get('limit');
  $skip = $app->request->get('skip');

  // Conta quantos registros entram na condição
  $table_count = Users::where('name','LIKE',"%{$description}%")->orwhere('last_name','LIKE',"%{$description}%")->count();
  $table_count = $table_count > 0 ? $table_count : 1;
  // Se recebeu um limit utiliza-o, caso contrário usa o limite máximo da tabela
  $limit = (!empty($limit)) ? $limit : $table_count;
  // Se recebeu um offset utiliza-o, caso contrário seta-o como 0
  $skip = (!empty($skip)) ? $skip : 0;

  $results = Users::where('name','LIKE',"%{$description}%")
                    ->orwhere('last_name','LIKE',"%{$description}%")
                    ->select(array('id', 'name', 'last_name','email','active'))
                    ->orderBy('id')
                    ->skip($skip)
                    ->take($limit)
                    ->get();


  $message = $results->count() . ' results';
  return helpers::jsonResponse(false, $message, $table_count, $results );

});

# Precisa ser um post pois eu passo os dados das empresas
$app->post('/v1/users/editor', $authentication, function() use ($app) {

  # Obtém os dados
  $request = \Slim\Slim::getInstance()->request();
  $user_rq = json_decode($request->getBody());

  $results = [];
  $description = $app->request->get('description');
  $description = (!empty($description)) ? $description : '';
  
  //var_dump($user_rq);

  if(isset($user_rq->enterprises) && !empty($user_rq->enterprises)){

    foreach ($user_rq->enterprises as $key => $value) {
      $enterprises[] = $value->id;
    }

    $enterprises = implode(',',$enterprises);

  }else{

    $enterprises = '0';

  }

  // Limite e Registros que serão 'pulados'
  $limit = $app->request->get('limit');
  $skip = $app->request->get('skip');

  // Conta quantos registros entram na condição
  $table_count = Users::join('person_enterprise','person.id','=','person_enterprise.person_fk')
                    ->where('name','LIKE',"%{$description}%")
                    ->whereRaw('enterprise_fk in (' . $enterprises . ')')
                    ->orwhere('last_name','LIKE',"%{$description}%")->count();
  $table_count = $table_count > 0 ? $table_count : 1;
  // Se recebeu um limit utiliza-o, caso contrário usa o limite máximo da tabela
  $limit = (!empty($limit)) ? $limit : $table_count;
  // Se recebeu um offset utiliza-o, caso contrário seta-o como 0
  $skip = (!empty($skip)) ? $skip : 0;

  $results = Users::join('person_enterprise','person.id','=','person_enterprise.person_fk')
                    ->where(function ($query) use ($description) {
                        $query
                              ->where('name','LIKE',"%{$description}%")
                              ->orwhere('last_name','LIKE',"%{$description}%");
                    })
                    ->whereRaw('enterprise_fk in (' . $enterprises . ')')
                    ->select(array('person.id', 'person.name', 'person.last_name','person.email','person.active'))
                    ->groupBy('person.id')
                    ->orderBy('person.id')
                    ->skip($skip)
                    ->take($limit)
                    ->get();


  $message = $results->count() . ' results';
  return helpers::jsonResponse(false, $message, $table_count, $results );

});

$app->post('/v1/users', $authentication, function() use ($app) {

  # Obtém os dados
  $request = \Slim\Slim::getInstance()->request();
  $user_rq = json_decode($request->getBody());

  # Verifica se já existe algum usuário com esse email
  $search = Users::where('email','=',$user_rq->email)->get();
  if(count($search) > 0) return helpers::jsonResponse(true, 'User not created', 0, 
    array('msg' => 'Usuário com esse email já existe'));

  $user = new Users;
  $user->name = trim($user_rq->name);
  $user->last_name = (!empty($user_rq->last_name)) ? trim($user_rq->last_name) : '';  # Não obrigatório
  $user->email = $user_rq->email;
  $user->type  = (isset($user_rq->type) && !empty($user_rq->type)) ? $user_rq->type : 'USER';
  $user->can_send_email  = (isset($user_rq->can_send_email) && !empty($user_rq->can_send_email)) ? $user_rq->can_send_email : false;
  $user->password = crypt($user_rq->password);

  $user->api_token = helpers::genToken();

  $user->active = (!empty($user_rq->active)) ? $user_rq->active : false;  # Não obrigatório

  if($user->save()){

    // Salva as empresas que o usuário pode acessar, caso existam
    if(isset($user_rq->selection) && !empty($user_rq->selection)){

      // Apaga os endereços cadastrados anteriormente
      $user_enterprise = Users_Enterprises::where('person_fk','=',$user->id);
      if(!empty($user_enterprise)) $user_enterprise->delete();

      foreach ($user_rq->selection as $key => $enterprise) {
        
        $user_enterprise = new Users_Enterprises;
        $user_enterprise->enterprise_fk = $enterprise->id;
        $user_enterprise->person_fk = $user->id;

        if(!$user_enterprise->save()){
          return  helpers::jsonResponse(true, 'User enterprises save failed', 0);
        }

      }

    }

    return  helpers::jsonResponse(false, 'User created', 0, array('id' => $user->id));
  }else{
    return  helpers::jsonResponse(true, 'User create failed', 0);
  }

});

$app->get('/v1/user/:id', $authentication, function($id) use ($app) {

  # Obtém um registro específico
  $user = Users::select(array('id', 'name', 'last_name','email','type','active', 'img'))->find($id);

  if(!empty($user)){

    $enterprises = Users_Enterprises::join('enterprise','enterprise.id','=','enterprise_fk')
                                    ->select('name','enterprise_fk as id')
                                    ->where('person_fk','=',$user->id)->get();
    $user->enterprises = $enterprises;

    return  helpers::jsonResponse(false, 'User found', 1, $user);
  }else{
    return  helpers::jsonResponse(true, 'User not found', 0);
  }

});

# Obtém as empresas vinculadas ao usuário
$app->get('/v1/user/:user/enterprises', $authentication, function($user) use ($app) {

  # Carrega as empresas relacionadas ao cliente
  $results = Users_Enterprises::join('enterprise','enterprise.id','=','enterprise_fk')
                                    ->where('person_fk','=',$user)
                                    ->get();

  # Obtém as formas de pagamento para cada item
  foreach ($results as $key => $value) {
    
    # Calcula o nome da imagem que será utilizada para classificação
    $class_number = helpers::classificationImage($value->classification);
    $value->classification_image = 'stars' . $class_number . '.png';

    # Carrega os comentários da empresa
    //$enterprise->comments = Comments::select('comment.id', 'person_fk', 'person_name as name',  'classification', 'text', 'comment.updated_at')
                          //->where('enterprise_fk','=',$enterprise->id)->get();

    $my_results[] = $value;

  }

  return  helpers::jsonResponse(false, 'User found', 1, $my_results);

});

$app->put('/v1/user/:id', $authentication, function($id) use ($app) {

  # Obtém os dados
  $request = \Slim\Slim::getInstance()->request();
  $user_rq = json_decode($request->getBody());

  $user = Users::find($id);
  $user->name = trim($user_rq->name);
  $user->last_name = (!empty($user_rq->last_name)) ? trim($user_rq->last_name) : '';  # Não obrigatório
  $user->email = $user_rq->email;
  $user->type  = (!empty($user_rq->type)) ? $user_rq->type : 'USER';
  $user->password = (!empty($user_rq->password)) ? crypt($user_rq->password) : $user->password; # Passa a si mesma se não veio uma nova
  
  # Não gera o token ao alterar o usuário
  # $user->api_token = helpers::genToken();

  $user->active = (!empty($user_rq->active)) ? $user_rq->active : false;  # Não obrigatório

 if($user->save()){

    // Salva as empresas que o usuário pode acessar, caso existam
    if(isset($user_rq->selection) && !empty($user_rq->selection)){

      // Apaga os endereços cadastrados anteriormente
      $user_enterprise = Users_Enterprises::where('person_fk','=',$user->id);
      if(!empty($user_enterprise)) $user_enterprise->delete();

      foreach ($user_rq->selection as $key => $enterprise) {
        
        $user_enterprise = new Users_Enterprises;
        $user_enterprise->enterprise_fk = $enterprise->id;
        $user_enterprise->person_fk = $user->id;

        if(!$user_enterprise->save()){
          return  helpers::jsonResponse(true, 'User enterprises save failed', 0);
        }

      }

    }

    return  helpers::jsonResponse(false, 'User saved', 0, array('id' => $user->id));
  }else{
    return  helpers::jsonResponse(true, 'User save failed', 0);
  }

});

$app->delete('/v1/user/:id', $authentication, function($id) use ($app) {

  # Obtém os dados

  $user = Users::find($id);

  if(!empty($user)){

    try{

      if($user->delete()){
        return  helpers::jsonResponse(false, 'User deleted', 1);
      }else{
        return  helpers::jsonResponse(true, 'User not deleted', 0);
      }

    }catch (Exception $e){

      return  helpers::jsonResponse(true, 'Não é possível deletar.', 1);

    }

  }else{

    # Caso o usuário não tenha sido encontrado
    return  helpers::jsonResponse(true, 'User not found to delete', 0);
  }

});

# Vincular empresa ao usuário
$app->post('/v1/user/:user/link/:enterprise', $authentication, function($user, $enterprise) use ($app) {

  $user_enterprise = new Users_Enterprises;
  $user_enterprise->person_fk = $user;
  $user_enterprise->enterprise_fk = $enterprise;

  if($user_enterprise->save()){

    # Retorna as empresas vinculadas ao usuário
    $enterprises = Users_Enterprises::join('enterprise','enterprise.id','=','enterprise_fk')
                                    ->select('name','enterprise_fk as id')
                                    ->where('person_fk','=',$user_enterprise->person_fk)->get();

    $user_enterprise->enterprises = $enterprises;

    return  helpers::jsonResponse(false, 'User linked to enterprise', 0, $user_enterprise);
  }else{
    return  helpers::jsonResponse(true, 'User link to enterprise failed', 0);
  }

});

# Desvincular empresa ao usuário
$app->post('/v1/user/:user/unlink/:enterprise', $authentication, function($user, $enterprise) use ($app) {

  $user_enterprise = Users_Enterprises::where('person_fk','=',$user)->where('enterprise_fk','=',$enterprise);

  if($user_enterprise->delete()){

    # Retorna as empresas vinculadas ao usuário
    $enterprises = Users_Enterprises::join('enterprise','enterprise.id','=','enterprise_fk')
                                    ->select('name','enterprise_fk as id')
                                    ->where('person_fk','=',$user)->get();

    $user_enterprise->enterprises = $enterprises;

    return  helpers::jsonResponse(false, 'User unlinked to enterprise', 0, $user_enterprise);
  }else{
    return  helpers::jsonResponse(true, 'User unlink to enterprise failed', 0);
  }

});

$app->post('/v1/user/login/', function() use ($app) {

   # Obtém os dados
  $request = \Slim\Slim::getInstance()->request();
  $user_rq = json_decode($request->getBody());

  $email = trim($user_rq->email);

  # Obtém um registro específico
  $users = Users::where('active','=',1)
          ->where('email','=',trim($email))
          ->where('type','=','ADMIN')
          ->select(array('id', 'name', 'password', 'last_name','email', 'api_token', 'active', 'img', 'type'))
          ->get();

  if(count($users) > 0){

    foreach ($users as $key => $user) {

      // Encripta para realizar a verificação
      $password = $user->password;

      if(crypt($user_rq->password, $password) == $password){

        if($user->type == 'ADMIN'){

          $devices = Devices::count();
          $stations = Enterprises::where('active','=','1')->count();
          $comments = Comments::count();
          $logs = Logs::orderBy('id','desc')->limit(20)->get();

          $user->devices_number = $devices;
          $user->stations_number = $stations;
          $user->comments_number = $comments;
          $user->logs = $logs;

        }

        // Limpa o password para não ser enviado
        $user->password = '';
        return  helpers::jsonResponse(false, 'User found', 1, $user);
      }
    }

    return  helpers::jsonResponse(true, 'User not found', 0);

  }else{
    return  helpers::jsonResponse(true, 'User not found', 0);
  }

});

# Login feito na parte do cliente, pode alterar apenas os dados de determinadas empresas
$app->post('/v1/user/login/editor', function() use ($app) {

   # Obtém os dados
  $request = \Slim\Slim::getInstance()->request();
  $user_rq = json_decode($request->getBody());

  $email = trim($user_rq->email);

  # Obtém um registro específico
  $users = Users::where('active','=',1)
          ->where('email','=',trim($email))
          ->whereRaw("type IN ('ADMIN','EDITOR','EDITOR_JUNIOR')")
          ->select(array('id', 'name', 'password', 'last_name','email', 'api_token', 'active', 'img', 'type'))
          ->get();

  if(count($users) > 0){

    foreach ($users as $key => $user) {

      // Encripta para realizar a verificação
      $password = $user->password;

      if(crypt($user_rq->password, $password) == $password){

        // Limpa o password para não ser enviado
        $user->password = '';

        # Carrega as empresas relacionadas ao cliente
        $enterprises = Users_Enterprises::join('enterprise','enterprise.id','=','enterprise_fk')
                                          ->select('enterprise.id','enterprise.name','enterprise.img','enterprise.active')
                                          ->where('person_fk','=',$user->id)
                                          ->get();

        $user->enterprises = $enterprises;

        return  helpers::jsonResponse(false, 'User found', 1, $user);
      }
    }

    return  helpers::jsonResponse(true, 'User not found', 0);

  }else{
    return  helpers::jsonResponse(true, 'User not found', 0);
  }

});

# Registrar dispositivo
$app->post('/v1/devices/register', function() use ($app) {

  $device = new Devices;

  $device->api_token = helpers::genToken();

  if($device->save()){
    return  helpers::jsonResponse(false, 'Device saved', 0, $device);
  }else{
    return  helpers::jsonResponse(true, 'Device save failed', 0);
  }

});

$app->post('/v1/devices/favorite/:enterprise/:device', $authentication, function($enterprise_id, $device_id) use ($app) {

  $favorite = new Favorites;

  $favorite->active = 1;
  $favorite->enterprise_fk = $enterprise_id;
  $favorite->device_fk = $device_id;

  if($favorite->save()){

    # Seleciona o nome da empresa que fará parte dos favoritos
    $enterprise = Enterprises::where('id','=',$favorite->enterprise_fk)->select('name')->get();
    $favorite->enterprise_name = $enterprise[0]->name;

    return  helpers::jsonResponse(false, 'Favorite saved', 0, $favorite);
  }else{
    return  helpers::jsonResponse(true, 'Favorite save failed', 0);
  }

});

$app->delete('/v1/devices/remove/favorite/:enterprise/:device', $authentication, function($enterprise_id, $device_id) use ($app) {

  $favorite = Favorites::where('enterprise_fk','=',$enterprise_id)->where('device_fk','=',$device_id);

  if($favorite->delete()){
    return  helpers::jsonResponse(false, 'Favorite deleted', 0, $favorite);
  }else{
    return  helpers::jsonResponse(true, 'Favorite delete failed', 0);
  }

});

# COMENTÁRIOS E VOTAÇÃO

$app->post('/v1/user/:user_id/enterprise/:station_id/rate', function($user_id, $station_id) use ($app) {

   # Obtém os dados
  $request = \Slim\Slim::getInstance()->request();
  $rate_rq = json_decode($request->getBody());

  $comment = new Comments();

  $comment->text = trim($rate_rq->comment);
  $comment->classification = trim($rate_rq->classification);
  $comment->enterprise_fk = $station_id;
  $comment->person_fk = $user_id;

  // Busca o nome do usuário para salvá-lo no comentário (melhoria de performance)
  $person = Users::select('name')->find($user_id);
  $comment->person_name = $person->name;

  if($comment->save()){

    # Refaz os cálculos de rating da empresa
    $enterprise = Enterprises::find($comment->enterprise_fk);
    
    $enterprise->totalClassified++;
    $enterprise->comments++;
    $enterprise->totalStars = $enterprise->totalStars + $comment->classification;
    $enterprise->classification = $enterprise->totalStars / $enterprise->totalClassified;

    $enterprise->save();

    # Carrega os comentários da empresa
    $comments = Comments::select('comment.id', 'person_fk', 'person_name as name',  'classification', 'text', 'comment.updated_at')
                          ->where('enterprise_fk','=',$comment->enterprise_fk)->get();

    return  helpers::jsonResponse(false, 'Comment created', 0, $comments);
  }else{
    return  helpers::jsonResponse(true, 'Comment create failed', 0);
  }

});

$app->put('/v1/user/rate/edit', function() use ($app) {

   # Obtém os dados
  $request = \Slim\Slim::getInstance()->request();
  $rate_rq = json_decode($request->getBody());

  $comment = Comments::find($rate_rq->id);

  $comment->text = trim($rate_rq->comment);
  # Guarda a classificação para uso posterior
  $old_classification = $comment->classification;
  $comment->classification = trim($rate_rq->classification);

  if($comment->save()){

    # Refaz os cálculos de rating da empresa
    $enterprise = Enterprises::find($comment->enterprise_fk);
    
    $enterprise->totalStars = ($enterprise->totalStars + $comment->classification) - $old_classification;
    $enterprise->classification = $enterprise->totalStars / $enterprise->totalClassified;
    $enterprise->save();

    # Carrega os comentários da empresa
    $comments = Comments::select('comment.id', 'person_fk', 'person_name as name',  'classification', 'text', 'comment.updated_at')
                          ->where('enterprise_fk','=',$comment->enterprise_fk)->get();

    return  helpers::jsonResponse(false, 'Comment created', 0, $comments);
  }else{
    return  helpers::jsonResponse(true, 'Comment create failed', 0);
  }

});

$app->delete('/v1/user/rate/:id', $authentication, function($id) use ($app) {

  $rate = Comments::find($id);
  $enterprise_fk = $rate->enterprise_fk;
  $classification = $rate->classification;

  if($rate->delete()){

    # Refaz os cálculos de rating da empresa
    $enterprise = Enterprises::find($enterprise_fk);
    
    $enterprise->totalStars = ($enterprise->totalStars - $classification);
    $enterprise->classification = $enterprise->totalStars / $enterprise->totalClassified;
    $enterprise->comments--;
    $enterprise->totalClassified--;
    $enterprise->save();

    # Carrega os comentários da empresa
    $comments = Comments::select('comment.id', 'person_fk', 'person_name as name',  'classification', 'text', 'comment.updated_at')
                          ->where('enterprise_fk','=',$enterprise_fk)->get();

    return  helpers::jsonResponse(false, 'Rate deleted', 0, $comments);
  }else{
    return  helpers::jsonResponse(true, 'Rate delete failed', 0);
  }

});

# UPLOAD DA IMAGEM
$app->post('/v1/user/:id/image/upload', $authentication, function($id) use ($app) {

  $dir = UPLOAD_DIR . '/user/';

  // Localiza o objeto em questão
  $user = Users::find($id);

  // Deleta a imagem anterior, caso exista
  if(!empty($user->img)){
    if(file_exists($dir . $user->img)){
      unlink($dir . $user->img);
      $user->img = '';
    }
  }

  // Gera o novo nome da imagem
  $name = 'logotype_' . rand(101, 999) . '_' . $id . '.jpg';

  // Se conseguimos mover o novo arquivo, seta o nome do mesmo no objeto
  if(move_uploaded_file($_FILES['file']['tmp_name'], $dir . $name)){
    $user->img = trim($name);
  }

  if($user->save()){
    return  helpers::jsonResponse(false, 'User image saved', 0);
  }else{
    return  helpers::jsonResponse(true, 'User image save failed', 0);
  }

});