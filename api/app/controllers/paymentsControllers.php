<?php

# Funções/Controllers da API

# VERSÃO 1 - V1

$app->get('/v1/payments', $authentication, function() use ($app) {

  $results = [];
  $description = $app->request->get('description');
  $description = (!empty($description)) ? $description : '';
  
  // Limite e Registros que serão 'pulados'
  $limit = $app->request->get('limit');
  $skip = $app->request->get('skip');

  // Conta quantos registros entram na condição
  $table_count = Payments::where('name','LIKE',"%{$description}%")->count();
  $table_count = $table_count > 0 ? $table_count : 1;
  // Se recebeu um limit utiliza-o, caso contrário usa o limite máximo da tabela
  $limit = (!empty($limit)) ? $limit : $table_count;
  // Se recebeu um offset utiliza-o, caso contrário seta-o como 0
  $skip = (!empty($skip)) ? $skip : 0;

  $results = Payments::where('name','LIKE',"%{$description}%")
                    ->select(array('id', 'name', 'img'))
                    ->orderBy('id')
                    ->skip($skip)
                    ->take($limit)
                    ->get();


  $message = $results->count() . ' results';
  return helpers::jsonResponse(false, $message, $table_count, $results );

});

$app->get('/v1/payments/mobile', $authentication, function() use ($app) {

  $results = [];
  $results = Payments::select(array('id', 'name'))
                    ->orderBy('id')
                    ->get();


  $message = $results->count() . ' results';
  return helpers::jsonResponse(false, $message, 0, $results );

});

$app->post('/v1/payments', $authentication, function() use ($app) {

  # Obtém os dados
  $request = \Slim\Slim::getInstance()->request();
  $payment_rq = json_decode($request->getBody());

  $payment = new Payments;
  $payment->name = trim($payment_rq->name);

  if($payment->save()){
    return  helpers::jsonResponse(false, 'Payment created', 0, array('id' => $payment->id));
  }else{
    return  helpers::jsonResponse(true, 'Payment create failed', 0);
  }

});

$app->get('/v1/payment/:id', $authentication, function($id) use ($app) {

  # Obtém um registro específico
  $payment = Payments::select(array('id', 'name', 'img'))->find($id);

  if(!empty($payment)){
    return  helpers::jsonResponse(false, 'Payment found', 1, $payment);
  }else{
    return  helpers::jsonResponse(true, 'Payment not found', 0);
  }

});

$app->put('/v1/payment/:id', $authentication, function($id) use ($app) {

  # Obtém os dados
  $request = \Slim\Slim::getInstance()->request();
  $payment_rq = json_decode($request->getBody());

  $payment = Payments::find($id);
  $payment->name = trim($payment_rq->name);

 if($payment->save()){
    return  helpers::jsonResponse(false, 'Payment saved', 0, array('id' => $payment->id));
  }else{
    return  helpers::jsonResponse(true, 'Payment save failed', 0);
  }

});

$app->delete('/v1/payment/:id', $authentication, function($id) use ($app) {

  # Obtém os dados

  $payment = Payments::find($id);

  if(!empty($payment)){

    try{

      if($payment->delete()){
        return  helpers::jsonResponse(false, 'Payment deleted', 1);
      }else{
        return  helpers::jsonResponse(true, 'Payment not deleted', 0);
      }

    }catch (Exception $e){

      return  helpers::jsonResponse(true, 'Não é possível deletar.', 1);

    }

  }else{

    # Caso o usuário não tenha sido encontrado
    return  helpers::jsonResponse(true, 'Payment not found to delete', 0);
  }

});

# UPLOAD DA IMAGEM
$app->post('/v1/payment/:id/image/upload', $authentication, function($id) use ($app) {

  $dir = UPLOAD_DIR . '/payment/';

  // Localiza o objeto em questão
  $payment = Payments::find($id);

  // Deleta a imagem anterior, caso exista
  if(!empty($payment->img)){
    if(file_exists($dir . $payment->img)){
      unlink($dir . $payment->img);
      $payment->img = '';
    }
  }

  // Gera o novo nome da imagem
  $name = 'logotype_' . rand(101, 999) . '_' . $id . '.jpg';

  // Se conseguimos mover o novo arquivo, seta o nome do mesmo no objeto
  if(move_uploaded_file($_FILES['file']['tmp_name'], $dir . $name)){
    $payment->img = trim($name);
  }

  if($payment->save()){
    return  helpers::jsonResponse(false, 'Payment logotype saved', 0);
  }else{
    return  helpers::jsonResponse(true, 'Payment logotype save failed', 0);
  }

});