<?php

# Funções/Controllers da API

# VERSÃO 1 - V1

$app->get('/v1/flags', $authentication, function() use ($app) {

  $results = [];
  $description = $app->request->get('description');
  $description = (!empty($description)) ? $description : '';
  
  // Limite e Registros que serão 'pulados'
  $limit = $app->request->get('limit');
  $skip = $app->request->get('skip');

  // Conta quantos registros entram na condição
  $table_count = Flags::where('name','LIKE',"%{$description}%")->orwhere('name_anp','LIKE',"%{$description}%")->count();
  $table_count = $table_count > 0 ? $table_count : 1;
  // Se recebeu um limit utiliza-o, caso contrário usa o limite máximo da tabela
  $limit = (!empty($limit)) ? $limit : $table_count;
  // Se recebeu um offset utiliza-o, caso contrário seta-o como 0
  $skip = (!empty($skip)) ? $skip : 0;

  $results = Flags::where('name','LIKE',"%{$description}%")->orwhere('name_anp','LIKE',"%{$description}%")
                    ->select(array('id', 'name', 'name_anp', 'img'))
                    ->orderBy('id')
                    ->skip($skip)
                    ->take($limit)
                    ->get();


  $message = $results->count() . ' results';
  return helpers::jsonResponse(false, $message, $table_count, $results );

});

$app->post('/v1/flags', $authentication, function() use ($app) {

  # Obtém os dados
  $request = \Slim\Slim::getInstance()->request();
  $flag_rq = json_decode($request->getBody());

  $flag = new Flags;
  $flag->name         = trim($flag_rq->name);
  $flag->name_anp     = (!empty($flag_rq->name_anp)) ? $flag_rq->name_anp : null;  # Não obrigatório
  $flag->description  = (!empty($flag_rq->description)) ? $flag_rq->description : null;  # Não obrigatório

  if($flag->save()){
    return  helpers::jsonResponse(false, 'Flag created', 0, array('id' => $flag->id));
  }else{
    return  helpers::jsonResponse(true, 'Flag create failed', 0);
  }

});

$app->get('/v1/flag/:id', $authentication, function($id) use ($app) {

  # Obtém um registro específico
  $flag = Flags::select(array('id', 'name', 'name_anp', 'description', 'img'))->find($id);

  if(!empty($flag)){
    return  helpers::jsonResponse(false, 'Flag found', 1, $flag);
  }else{
    return  helpers::jsonResponse(true, 'Flag not found', 0);
  }

});

$app->put('/v1/flag/:id', $authentication, function($id) use ($app) {

  # Obtém os dados
  $request = \Slim\Slim::getInstance()->request();
  $flag_rq = json_decode($request->getBody());

  $flag = Flags::find($id);
  $flag->name = trim($flag_rq->name);
  $flag->name_anp     = (!empty($flag_rq->name_anp)) ? $flag_rq->name_anp : null;  # Não obrigatório
  $flag->description  = (!empty($flag_rq->description)) ? $flag_rq->description : null;  # Não obrigatório

 if($flag->save()){
    return  helpers::jsonResponse(false, 'Flag saved', 0, array('id' => $flag->id));
  }else{
    return  helpers::jsonResponse(true, 'Flag save failed', 0);
  }

});

$app->delete('/v1/flag/:id', $authentication, function($id) use ($app) {

  # Obtém os dados

  $flag = Flags::find($id);

  if(!empty($flag)){

    try{

      if($flag->delete()){
        return  helpers::jsonResponse(false, 'Flag deleted', 1);
      }else{
        return  helpers::jsonResponse(true, 'Flag not deleted', 0);
      }

    }catch (Exception $e){

      return  helpers::jsonResponse(true, 'Não é possível deletar.', 1);

    }

  }else{

    # Caso o usuário não tenha sido encontrado
    return  helpers::jsonResponse(true, 'Flag not found to delete', 0);
  }

});

# UPLOAD DA IMAGEM
$app->post('/v1/flag/:id/image/upload', $authentication, function($id) use ($app) {

  $dir = UPLOAD_DIR . '/flag/';

  // Localiza o objeto em questão
  $flag = Flags::find($id);

  // Deleta a imagem anterior, caso exista
  if(!empty($flag->img)){
    if(file_exists($dir . $flag->img)){
      unlink($dir . $flag->img);
      $flag->img = '';
    }
  }

  // Gera o novo nome da imagem
  $name = 'logotype_' . rand(101, 999) . '_' . $id . '.jpg';

  // Se conseguimos mover o novo arquivo, seta o nome do mesmo no objeto
  if(move_uploaded_file($_FILES['file']['tmp_name'], $dir . $name)){
    $flag->img = trim($name);
  }

  if($flag->save()){

    # Vamos atualizar todas as empresas que possuam essa bandeira com o nome da imagem
    Enterprises::where('flag_fk', '=', $id)->update(
      array(
        'flag_img' => $flag->img,
      )
    );

    return  helpers::jsonResponse(false, 'Flag logotype saved', 0);
  }else{
    return  helpers::jsonResponse(true, 'Flag logotype save failed', 0);
  }

});