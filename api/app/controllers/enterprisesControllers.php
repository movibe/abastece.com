<?php

# Funções/Controllers da API

# VERSÃO 1 - V1
use Illuminate\Database\Capsule\Manager as Capsule;

$app->get('/v1/enterprises', $authentication, function() use ($app) {

  $results = [];
  $description = $app->request->get('description');
  
  // Limite e Registros que serão 'pulados'
  $limit  = $app->request->get('limit');
  $skip   = $app->request->get('skip');
  $except = $app->request->get('except');
  $order  = $app->request->get('order');

  // Verifica se vai fazer exceção de id
  $except = (!empty($except)) ? $except : '0';
  $order  = (!empty($order)) ? $order : 'id';

  // Melhor mantar assim por performance
  if ( $description ) {
    
    // Conta quantos registros entram na condição
    $table_count = Enterprises::where('name','LIKE',"%{$description}%")->count();
    // Se recebeu um limit utiliza-o, caso contrário usa o limite máximo da tabela
    $limit = (!empty($limit)) ? $limit : $table_count;
    // Se recebeu um offset utiliza-o, caso contrário seta-o como 0
    $skip = (!empty($skip)) ? $skip : 0;

    $results = Enterprises::where('name','LIKE',"%{$description}%")
                      ->where('id','!=',"$except")
                      ->select(array('id', 'name', 'street', 'number', 'city', 'district'))
                      ->orderBy($order)
                      ->skip($skip)
                      ->take($limit)
                      ->get();

  } else {

    $table_count = Enterprises::count();
    $limit = (!empty($limit)) ? $limit : $table_count;
    
    $results = Enterprises::select(array('id', 'name', 'street', 'number', 'city', 'district'))
                      ->where('id','!=',$except)
                      ->orderBy($order)
                      ->skip($skip)
                      ->take($limit)
                      ->get();
  }

  $message = $results->count() . ' results';
  return helpers::jsonResponse(false, $message, $table_count, $results );

});

$app->get('/v1/enterprises/mobile', $authentication, function() use ($app) {

  $results = [];
  
  // Limite e Registros que serão 'pulados'
  $limit  = $app->request->get('limit');
  $skip   = $app->request->get('skip');
  $bounds = $app->request->get('bounds');
  $lat    = $app->request->get('lat');
  $lon    = $app->request->get('lon');
  //$order  = $app->request->get('order');

  // Limites da cidade
  $bounds_r = explode(',', $bounds, 4);

  $table_count = 0;
  //$table_count = Enterprises::count();
  //$limit = (!empty($limit)) ? $limit : $table_count;
  
  $results = Enterprises::select(array('enterprise.id', 'enterprise.name', 'classification', 'flag_fk',
      'street', 'number', 'city', 'district', 
      'hasGasoline', 'hasGasolineLeaded', 'hasGasolinePremium', 'hasEthanol', 
      'hasDiesel', 'hasDieselS10', 'hasNaturalGas', 'priceGasoline', 'priceGasolineLeaded', 'priceGasolinePremium', 
      'priceEthanol', 'priceDiesel', 'priceDieselS10', 'priceNaturalGas',
      'flag_img', 'latitude', 'longitude'
      ))
      # Calcula a distância entre posto e dispositivo
      ->selectRaw(
        '
          (
          111.1111 *
          DEGREES(ACOS(COS(RADIANS(' . $lat . '))
             * COS(RADIANS(latitude))
             * COS(RADIANS(' . $lon . ' - longitude))
             + SIN(RADIANS(' . $lat . '))
             * SIN(RADIANS(latitude))))
          ) as distance
        '
      )
      # Busca os postos dentro dos limites da cidade
      ->whereRaw("(CASE WHEN {$bounds_r[0]} < {$bounds_r[2]}
        THEN latitude BETWEEN $bounds_r[0] AND {$bounds_r[2]}
        ELSE latitude BETWEEN {$bounds_r[2]} AND {$bounds_r[0]}
        END) 
        AND
        (CASE WHEN {$bounds_r[1]} < {$bounds_r[3]}
                THEN longitude BETWEEN {$bounds_r[1]} AND {$bounds_r[3]}
                ELSE longitude BETWEEN {$bounds_r[3]} AND {$bounds_r[1]}
        END)")
      ->where('active','=','1')
      //->skip($skip)
      //->take($limit)
      ->get();
  
  # Obtém as formas de pagamento para cada item
  foreach ($results as $key => $value) {
    
    # Calcula o nome da imagem que será utilizada para classificação
    $class_number = helpers::classificationImage($value->classification);
    $value->classification_image = 'stars' . $class_number . '.png';

    $value->payments = Enterprises_Payments::join('payment','payment.id','=','payment_fk')
                                    ->where('enterprise_fk','=',$value->id)
                                    ->select('payment.id','payment.img','payment.name')
                                    ->get();

    $my_results[] = $value;

  }

  $message = $results->count() . ' results';
  return helpers::jsonResponse(false, $message, $table_count, $my_results );

});

$app->get('/v1/enterprises/mobile/map', $authentication, function() use ($app) {

  $results = [];
  
  // Limite e Registros que serão 'pulados'
  $limit  = $app->request->get('limit');
  $skip   = $app->request->get('skip');
  $bounds = $app->request->get('bounds');
  $lat    = $app->request->get('lat');
  $lon    = $app->request->get('lon');
  //$order  = $app->request->get('order');

  // Limites da cidade
  $bounds_r = explode(',', $bounds, 4);

  $table_count = 0;
  
  $results = Enterprises::select(array('enterprise.id', 'enterprise.name', 'classification', 'flag_fk',
      'street', 'number', 'city', 'district',
      'flag_img', 'latitude', 'longitude'
      ))
      # Calcula a distância entre posto e dispositivo
      ->selectRaw(
        '
          (
          111.1111 *
          DEGREES(ACOS(COS(RADIANS(' . $lat . '))
             * COS(RADIANS(latitude))
             * COS(RADIANS(' . $lon . ' - longitude))
             + SIN(RADIANS(' . $lat . '))
             * SIN(RADIANS(latitude))))
          ) as distance
        '
      )
      # Busca os postos dentro dos limites da cidade
      ->whereRaw("(CASE WHEN {$bounds_r[0]} < {$bounds_r[2]}
        THEN latitude BETWEEN $bounds_r[0] AND {$bounds_r[2]}
        ELSE latitude BETWEEN {$bounds_r[2]} AND {$bounds_r[0]}
        END) 
        AND
        (CASE WHEN {$bounds_r[1]} < {$bounds_r[3]}
                THEN longitude BETWEEN {$bounds_r[1]} AND {$bounds_r[3]}
                ELSE longitude BETWEEN {$bounds_r[3]} AND {$bounds_r[1]}
        END)")
      ->where('active','=','1')
      ->get();


  $message = $results->count() . ' results';
  return helpers::jsonResponse(false, $message, $table_count, $results );

});

$app->post('/v1/enterprises', $authentication, function() use ($app) {

  # Obtém os dados
  $request = \Slim\Slim::getInstance()->request();
  $enterprise_rq = json_decode($request->getBody());

  $enterprise = new Enterprises;
  
  $enterprise->name = trim($enterprise_rq->name);
  $enterprise->active = (!empty($enterprise_rq->active)) ? $enterprise_rq->active : false;  # Padrão true
  $enterprise->description = (!empty($enterprise_rq->description)) ? $enterprise_rq->description : null;  # Não obrigatório
  $enterprise->details     = (!empty($enterprise_rq->details)) ? $enterprise_rq->details : null;  # Não obrigatório
  $enterprise->inscription = (!empty($enterprise_rq->inscription)) ? $enterprise_rq->inscription : null;  # Não obrigatório
  $enterprise->flag_fk = trim($enterprise_rq->flag_fk);

  # Obtém o nome da imagem da bandeira para salvá-la
  $flag = Flags::select('img')->where('id','=',$enterprise->flag_fk)->get();
  $enterprise->flag_img = $flag[0]->img;

  $enterprise->email = (!empty($enterprise_rq->email)) ? $enterprise_rq->email : null;  # Não obrigatório
  $enterprise->phone = (!empty($enterprise_rq->phone)) ? $enterprise_rq->phone : null;  # Não obrigatório
  $enterprise->url = (!empty($enterprise_rq->url)) ? $enterprise_rq->url : null;  # Não obrigatório
  $enterprise->facebook_url = (!empty($enterprise_rq->facebook_url)) ? $enterprise_rq->facebook_url : null;  # Não obrigatório
  $enterprise->street = trim($enterprise_rq->street);
  $enterprise->number = trim($enterprise_rq->number);
  $enterprise->complement = (!empty($enterprise_rq->complement)) ? $enterprise_rq->complement : null;  # Não obrigatório
  $enterprise->postal = (!empty($enterprise_rq->postal)) ? $enterprise_rq->postal : null;  # Não obrigatório
  $enterprise->country = trim($enterprise_rq->country);  # Não obrigatório
  $enterprise->state = (!empty($enterprise_rq->state)) ? $enterprise_rq->state : null;  # Não obrigatório
  $enterprise->city = trim($enterprise_rq->city);  # Não obrigatório
  $enterprise->district = (!empty($enterprise_rq->district)) ? $enterprise_rq->district : null;  # Não obrigatório
  $enterprise->phone = (!empty($enterprise_rq->phone)) ? $enterprise_rq->phone : null;  # Não obrigatório
  $enterprise->latitude = (!empty($enterprise_rq->latitude)) ? $enterprise_rq->latitude : null;  # Não obrigatório
  $enterprise->longitude = (!empty($enterprise_rq->longitude)) ? $enterprise_rq->longitude : null;  # Não obrigatório
  $enterprise->time_initial = (!empty($enterprise_rq->time_initial)) ? $enterprise_rq->time_initial : null;  # Não obrigatório
  $enterprise->time_final = (!empty($enterprise_rq->time_final)) ? $enterprise_rq->time_final : null;  # Não obrigatório

  # Combustíveis
  $enterprise->hasGasoline     = (!empty($enterprise_rq->hasGasoline)) ? $enterprise_rq->hasGasoline : false;  # Não obrigatório
  $enterprise->priceGasoline   = (!empty($enterprise_rq->priceGasoline)) ? 
                               str_replace(',', '.', $enterprise_rq->priceGasoline) : 0; # Não obrigatório

  $enterprise->hasGasolineLeaded     = (!empty($enterprise_rq->hasGasolineLeaded)) ? $enterprise_rq->hasGasolineLeaded : false;  # Não obrigatório
  $enterprise->priceGasolineLeaded   = (!empty($enterprise_rq->priceGasolineLeaded)) ? 
                               str_replace(',', '.', $enterprise_rq->priceGasolineLeaded) : 0; # Não obrigatório

  $enterprise->hasGasolinePremium     = (!empty($enterprise_rq->hasGasolinePremium)) ? $enterprise_rq->hasGasolinePremium : false;  # Não obrigatório
  $enterprise->priceGasolinePremium   = (!empty($enterprise_rq->priceGasolinePremium)) ? 
                               str_replace(',', '.', $enterprise_rq->priceGasolinePremium) : 0; # Não obrigatório

  $enterprise->hasEthanol     = (!empty($enterprise_rq->hasEthanol)) ? $enterprise_rq->hasEthanol : false;  # Não obrigatório
  $enterprise->priceEthanol   = (!empty($enterprise_rq->priceEthanol)) ? 
                               str_replace(',', '.', $enterprise_rq->priceEthanol) : 0; # Não obrigatório

  $enterprise->hasDiesel     = (!empty($enterprise_rq->hasDiesel)) ? $enterprise_rq->hasDiesel : false;  # Não obrigatório
  $enterprise->priceDiesel   = (!empty($enterprise_rq->priceDiesel)) ? 
                               str_replace(',', '.', $enterprise_rq->priceDiesel) : 0; # Não obrigatório

  $enterprise->hasDieselS10     = (!empty($enterprise_rq->hasDieselS10)) ? $enterprise_rq->hasDieselS10 : false;  # Não obrigatório
  $enterprise->priceDieselS10   = (!empty($enterprise_rq->priceDieselS10)) ? 
                               str_replace(',', '.', $enterprise_rq->priceDieselS10) : 0; # Não obrigatório

  $enterprise->hasNaturalGas     = (!empty($enterprise_rq->hasNaturalGas)) ? $enterprise_rq->hasNaturalGas : false;  # Não obrigatório
  $enterprise->priceNaturalGas   = (!empty($enterprise_rq->priceNaturalGas)) ? 
                               str_replace(',', '.', $enterprise_rq->priceNaturalGas) : 0; # Não obrigatório

  // Ao inserir pela primeira vez adiciona a data atual como atualização do combustível
  $enterprise->fuel_updated_at = date('Y-m-d H:i:s');

  if($enterprise->save()){

    # Cria o LOG
    $person_id = $app->request->headers->get('HTTP_IDENT');
    helpers::log('Cadastro do posto: ' . $enterprise->name, $person_id, 'INSERT', $enterprise->id, 'enterprise');

    return  helpers::jsonResponse(false, 'Enterprise created', 0, array('id' => $enterprise->id));
  }else{
    return  helpers::jsonResponse(true, 'Enterprise create failed', 0);
  }

});

$app->post('/v1/enterprises/populate', function() use ($app) {

  /*set_time_limit(0);

  function GetRandomValue($min, $max) 
  { 
    $range = $max-$min; 
    $num = $min + $range * mt_rand(0, 32767)/32767; 
     
    $num = round($num, 6); 
     
    return ((float) $num); 
  } 
  
  for($i = 0; $i < 200000; $i++){

    $enterprise = new Enterprises;

    $enterprise->name = 'Posto ' . $i;
    $enterprise->active = true;  # Padrão true
    $enterprise->flag_fk = mt_rand(1, 6);
    $enterprise->latitude = GetRandomValue(-33.750382, 5.271786);  # Não obrigatório
    $enterprise->longitude = GetRandomValue(-73.982817, -29.345024);  # Não obrigatório

    # Combustíveis
    $enterprise->hasGasoline     = true;  # Não obrigatório
    $enterprise->priceGasoline   = GetRandomValue(1,3); # Não obrigatório

    $enterprise->save();
    
    
  }*/

});

$app->get('/v1/enterprise/:id', $authentication, function($id) use ($app) {

  # Obtém um registro específico
  $enterprise = Enterprises::select(
    array('id', 'name', 'active', 'img', 'description', 'details', 'inscription', 'flag_fk', 'email', 'phone', 'url', 'facebook_url', 'credit',
      'street', 'number', 'complement', 'postal', 'country', 'state', 'city', 'district', 'latitude', 'longitude', 
      'time_initial', 'time_final', 'hasGasoline', 'hasGasolineLeaded', 'hasGasolinePremium', 'hasEthanol', 
      'hasDiesel', 'hasDieselS10', 'hasNaturalGas', 'priceGasoline', 'priceGasolineLeaded', 'priceGasolinePremium', 
      'priceEthanol', 'priceDiesel', 'priceDieselS10', 'priceNaturalGas'))->find($id);

  if(!empty($enterprise)){

    # Obtém as formas de pagamento cadastradas para a empresa
    $enterprise->payments = Enterprises_Payments::where('enterprise_fk','=',$enterprise->id)
                            ->select('payment_fk as id')->get();

    return  helpers::jsonResponse(false, 'Enterprise found', 1, $enterprise);
  }else{
    return  helpers::jsonResponse(true, 'Enterprise not found', 0);
  }

});

$app->get('/v1/enterprise/:id/mobile', $authentication, function($id) use ($app) {

  # Obtém um registro específico
  $enterprise = Enterprises::select(
    array('enterprise.id', 'enterprise.name', 'active', 'enterprise.img', 'enterprise.description', 'flag_fk', 'email', 'phone',
      'classification','comments as comments_number', 'totalClassified',
      'street', 'number', 'complement', 'city', 'district', 'latitude', 'longitude', 
      'time_initial', 'time_final', 'hasGasoline', 'hasGasolineLeaded', 'hasGasolinePremium', 'hasEthanol', 
      'hasDiesel', 'hasDieselS10', 'hasNaturalGas', 'priceGasoline', 'priceGasolineLeaded', 'priceGasolinePremium', 
      'priceEthanol', 'priceDiesel', 'priceDieselS10', 'priceNaturalGas', 'flag_img', 'enterprise.updated_at', 'enterprise.fuel_updated_at'))->find($id);

  if(!empty($enterprise)){

    # Calcula o nome da imagem que será utilizada para classificação
    $class_number = helpers::classificationImage($enterprise->classification);
    $enterprise->classification_image = 'stars' . $class_number . '.png';

    # Carrega os comentários da empresa
    $enterprise->comments = Comments::select('comment.id', 'person_fk', 'person_name as name',  'classification', 'text', 'comment.updated_at')
                          ->where('enterprise_fk','=',$enterprise->id)->get();

    # Obtém as formas de pagamento cadastradas para a empresa
    $enterprise->payments = Enterprises_Payments::join('payment','payment.id','=','enterprise_payment.payment_fk')
                            ->where('enterprise_fk','=',$enterprise->id)
                            ->select('img')->get();

    return  helpers::jsonResponse(false, 'Enterprise found', 1, $enterprise);
  }else{
    return  helpers::jsonResponse(true, 'Enterprise not found', 0);
  }

});

$app->get('/v1/enterprise/:id/comments', $authentication, function($id) use ($app) {

  # Carrega os comentários da empresa
  $comments = Comments::join('person','person.id','=','comment.person_fk')
                        ->select('comment.id', 'person_fk', 'person.name',  'classification', 'text', 'comment.updated_at')
                        ->where('enterprise_fk','=',$id)->get();

  if(!empty($comments)){
    return  helpers::jsonResponse(false, 'Comments found', 1, $comments);
  }else{
    return  helpers::jsonResponse(true, 'Comments not found', 0);
  }

});

$app->put('/v1/enterprise/:id', $authentication, function($id) use ($app) {

  # Obtém os dados
  $request = \Slim\Slim::getInstance()->request();
  $enterprise_rq = json_decode($request->getBody());

  $enterprise = Enterprises::find($id);

  $enterprise->name = trim($enterprise_rq->name);
  $enterprise->active = (!empty($enterprise_rq->active)) ? $enterprise_rq->active : false;  # Padrão true
  $enterprise->description = (!empty($enterprise_rq->description)) ? $enterprise_rq->description : null;  # Não obrigatório
  $enterprise->details     = (!empty($enterprise_rq->details)) ? $enterprise_rq->details : $enterprise->details;  # Não obrigatório
  $enterprise->inscription = (!empty($enterprise_rq->inscription)) ? $enterprise_rq->inscription : null;  # Não obrigatório
  $enterprise->flag_fk = trim($enterprise_rq->flag_fk);

  # Obtém o nome da imagem da bandeira para salvá-la
  $flag = Flags::select('img')->where('id','=',$enterprise->flag_fk)->get();
  $enterprise->flag_img = $flag[0]->img;

  $enterprise->email = (!empty($enterprise_rq->email)) ? $enterprise_rq->email : null;  # Não obrigatório
  $enterprise->phone = (!empty($enterprise_rq->phone)) ? $enterprise_rq->phone : null;  # Não obrigatório
  $enterprise->url = (!empty($enterprise_rq->url)) ? $enterprise_rq->url : null;  # Não obrigatório
  $enterprise->facebook_url = (!empty($enterprise_rq->facebook_url)) ? $enterprise_rq->facebook_url : null;  # Não obrigatório
  $enterprise->street = trim($enterprise_rq->street);
  $enterprise->number = trim($enterprise_rq->number);
  $enterprise->complement = (!empty($enterprise_rq->complement)) ? $enterprise_rq->complement : null;  # Não obrigatório
  $enterprise->postal = (!empty($enterprise_rq->postal)) ? $enterprise_rq->postal : null;  # Não obrigatório
  $enterprise->country = trim($enterprise_rq->country);  # Não obrigatório
  $enterprise->state = (!empty($enterprise_rq->state)) ? $enterprise_rq->state : null;  # Não obrigatório
  $enterprise->city = trim($enterprise_rq->city);  # Não obrigatório
  $enterprise->district = (!empty($enterprise_rq->district)) ? $enterprise_rq->district : null;  # Não obrigatório
  $enterprise->phone = (!empty($enterprise_rq->phone)) ? $enterprise_rq->phone : null;  # Não obrigatório
  $enterprise->latitude = (!empty($enterprise_rq->latitude)) ? $enterprise_rq->latitude : null;  # Não obrigatório
  $enterprise->longitude = (!empty($enterprise_rq->longitude)) ? $enterprise_rq->longitude : null;  # Não obrigatório
  $enterprise->time_initial = (!empty($enterprise_rq->time_initial)) ? $enterprise_rq->time_initial : null;  # Não obrigatório
  $enterprise->time_final = (!empty($enterprise_rq->time_final)) ? $enterprise_rq->time_final : null;  # Não obrigatório

  # Combustíveis
  $enterprise->hasGasoline     = (!empty($enterprise_rq->hasGasoline)) ? $enterprise_rq->hasGasoline : false;  # Não obrigatório
  $enterprise->priceGasoline   = (!empty($enterprise_rq->priceGasoline)) ? 
                               str_replace(',', '.', $enterprise_rq->priceGasoline) : 0; # Não obrigatório

  $enterprise->hasGasolineLeaded     = (!empty($enterprise_rq->hasGasolineLeaded)) ? $enterprise_rq->hasGasolineLeaded : false;  # Não obrigatório
  $enterprise->priceGasolineLeaded   = (!empty($enterprise_rq->priceGasolineLeaded)) ? 
                               str_replace(',', '.', $enterprise_rq->priceGasolineLeaded) : 0; # Não obrigatório

  $enterprise->hasGasolinePremium     = (!empty($enterprise_rq->hasGasolinePremium)) ? $enterprise_rq->hasGasolinePremium : false;  # Não obrigatório
  $enterprise->priceGasolinePremium   = (!empty($enterprise_rq->priceGasolinePremium)) ? 
                               str_replace(',', '.', $enterprise_rq->priceGasolinePremium) : 0; # Não obrigatório

  $enterprise->hasEthanol     = (!empty($enterprise_rq->hasEthanol)) ? $enterprise_rq->hasEthanol : false;  # Não obrigatório
  $enterprise->priceEthanol   = (!empty($enterprise_rq->priceEthanol)) ? 
                               str_replace(',', '.', $enterprise_rq->priceEthanol) : 0; # Não obrigatório

  $enterprise->hasDiesel     = (!empty($enterprise_rq->hasDiesel)) ? $enterprise_rq->hasDiesel : false;  # Não obrigatório
  $enterprise->priceDiesel   = (!empty($enterprise_rq->priceDiesel)) ? 
                               str_replace(',', '.', $enterprise_rq->priceDiesel) : 0; # Não obrigatório

  $enterprise->hasDieselS10     = (!empty($enterprise_rq->hasDieselS10)) ? $enterprise_rq->hasDieselS10 : false;  # Não obrigatório
  $enterprise->priceDieselS10   = (!empty($enterprise_rq->priceDieselS10)) ? 
                               str_replace(',', '.', $enterprise_rq->priceDieselS10) : 0; # Não obrigatório

  $enterprise->hasNaturalGas     = (!empty($enterprise_rq->hasNaturalGas)) ? $enterprise_rq->hasNaturalGas : false;  # Não obrigatório
  $enterprise->priceNaturalGas   = (!empty($enterprise_rq->priceNaturalGas)) ? 
                               str_replace(',', '.', $enterprise_rq->priceNaturalGas) : 0; # Não obrigatório

  # Caso esteja salvando os preços, salva a data do preço
  $mode = $app->request->get('mode');
  $mode    = (!empty($mode)) ? $mode : 'normal';
  if($mode == 'prices'){

    $enterprise->fuel_updated_at = date('Y-m-d H:i:s');

  }

  if($enterprise->save()){

    // Apaga os endereços cadastrados anteriormente
    $enterprise_payments = Enterprises_Payments::where('enterprise_fk','=',$enterprise->id);
    if(!empty($enterprise_payments)) $enterprise_payments->delete();

    // Salva os endereços disponíveis para a promoção
    foreach($enterprise_rq->selection as $key=>$payment){

      $enterprise_payment = new Enterprises_Payments;
      $enterprise_payment->enterprise_fk = $enterprise->id;
      $enterprise_payment->payment_fk = $payment->id;

      if(!$enterprise_payment->save()){
        return  helpers::jsonResponse(true, 'Enterprise payments save failed', 0);
      }

    }

    # Cria o LOG
    $person_id = $app->request->headers->get('HTTP_IDENT');
    if($mode == 'prices'){
      helpers::log('Alteração de preços no posto: ' . $enterprise->name, $person_id, 'UPDATE', $enterprise->id, 'enterprise');
    }else{
      helpers::log('Alteração do posto: ' . $enterprise->name, $person_id, 'UPDATE', $enterprise->id, 'enterprise');
    }

    return  helpers::jsonResponse(false, 'Enterprise saved', 0, array('id' => $enterprise->id));
  }else{
    return  helpers::jsonResponse(true, 'Enterprise save failed', 0);
  }

});

$app->delete('/v1/enterprise/:id', $authentication, function($id) use ($app) {

  # Obtém os dados

  $enterprise = Enterprises::find($id);

  if(!empty($enterprise)){

    try{

      if($enterprise->delete()){
        return  helpers::jsonResponse(false, 'Enterprise deleted', 1);
      }else{
        return  helpers::jsonResponse(true, 'Enterprise not deleted', 0);
      }

    }catch (Exception $e){

      return  helpers::jsonResponse(true, 'Não é possível deletar pois está em uso. Verifique as relações.', 1);

    }

  }else{

    # Caso o usuário não tenha sido encontrado
    return  helpers::jsonResponse(true, 'Enterprise not found to delete', 0);
  }

});

# UPLOAD DA IMAGEM

$app->post('/v1/enterprise/:id/image/upload', $authentication, function($id) use ($app) {

  $dir = UPLOAD_DIR . '/enterprise/';

  // Localiza o objeto em questão
  $enterprise = Enterprises::find($id);

  // Deleta a imagem anterior, caso exista
  if(!empty($enterprise->img)){
    if(file_exists($dir . $enterprise->img)){
      unlink($dir . $enterprise->img);
      $enterprise->img = '';
    }
  }

  // Gera o novo nome da imagem
  $name = 'logotype_' . rand(101, 999) . '_' . $id . '.jpg';

  // Se conseguimos mover o novo arquivo, seta o nome do mesmo no objeto
  if(move_uploaded_file($_FILES['file']['tmp_name'], $dir . $name)){
    $enterprise->img = trim($name);
  }

  if($enterprise->save()){
    return  helpers::jsonResponse(false, 'Enterprise logotype saved', 0);
  }else{
    return  helpers::jsonResponse(true, 'Enterprise logotype save failed', 0);
  }

});

# SERVIÇOS

$app->get('/v1/enterprise/:id/services', $authentication, function($id) use ($app) {

  $id = is_numeric($id) ? $id : 0;

  # Obtém um registro específico
  $services = Services::where('enterprise_fk','=',$id)->orderBy('id')->get();

  if(!empty($services)){
    return  helpers::jsonResponse(false, 'Services found', 1, $services);
  }else{
    return  helpers::jsonResponse(true, 'Services not found', 0);
  }

});

$app->post('/v1/enterprise/:id/services', $authentication, function($id) use ($app) {

  # Obtém os dados
  $request = \Slim\Slim::getInstance()->request();
  $service_rq = json_decode($request->getBody());

  $service = new Services;
  
  $service->name = trim($service_rq->name);
  $service->description = (!empty($service_rq->description)) ? $service_rq->description : null;  # Não obrigatório
  $service->enterprise_fk = $id;

  if($service->save()){
    return  helpers::jsonResponse(false, 'Service created', 0);
  }else{
    return  helpers::jsonResponse(true, 'Service create failed', 0);
  }

});

$app->put('/v1/enterprise/service/:id', $authentication, function($id) use ($app) {

  # Obtém os dados
  $request = \Slim\Slim::getInstance()->request();
  $service_rq = json_decode($request->getBody());

  $service = Services::find($id);
  
  $service->name = trim($service_rq->name);
  $service->description = (!empty($service_rq->description)) ? $service_rq->description : null;  # Não obrigatório

  if($service->save()){
    return  helpers::jsonResponse(false, 'Service saved', 0);
  }else{
    return  helpers::jsonResponse(true, 'Service save failed', 0);
  }

});

$app->delete('/v1/enterprise/service/:id', $authentication, function($id) use ($app) {

  # Obtém os dados

  $service = Services::find($id);

  if(!empty($service)){

    if($service->delete()){
      return  helpers::jsonResponse(false, 'Service deleted', 1);
    }else{
      return  helpers::jsonResponse(true, 'Service not deleted', 0);
    }

  }else{

    # Caso o usuário não tenha sido encontrado
    return  helpers::jsonResponse(true, 'Service not found to delete', 0);
  }

});

$app->post('/v1/enterprise/service/:id/image/upload', $authentication, function($id) use ($app) {

  $dir = UPLOAD_DIR . '/service/';

  // Localiza o objeto em questão
  $service = Services::find($id);

  // Deleta a imagem anterior, caso exista
  if(!empty($service->img)){
    if(file_exists($dir . $service->img)){
      unlink($dir . $service->img);
      $service->img = '';
    }
  }

  // Gera o novo nome da imagem
  $name = 'service_' . rand(101, 999) . '_' . $id . '.jpg';

  // Se conseguimos mover o novo arquivo, seta o nome do mesmo no objeto
  if(move_uploaded_file($_FILES['file']['tmp_name'], $dir . $name)){
    $service->img = trim($name);
  }

  if($service->save()){
    return  helpers::jsonResponse(false, 'Service image saved', 0);
  }else{
    return  helpers::jsonResponse(true, 'Service image save failed', 0);
  }

});