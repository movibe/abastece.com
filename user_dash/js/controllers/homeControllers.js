angular.module('admin.controllers')

.controller('ApplicationController', function($scope, $rootScope, $location){

	$scope.logout = function(){

		$rootScope.isLogged = false;
		$location.path('/login');

	}

	$scope.shownItem = null;
	$scope.toggleItem = function(item) {
	    if ($scope.isItemShown(item)) {
	      $scope.shownItem = null;
	    } else {
	      $scope.shownItem = item;
	    }
	};
	$scope.isItemShown = function(item) {
	    return $scope.shownItem === item;
	};

})

.controller('HomeController', function($scope, $rootScope, $location, Users){

	if($rootScope.intern_user){

		Users.getEnterprises($rootScope.intern_user.id).then(function(s){

			$scope.enterprises = s.data.data;

		}, function(e){

			toastr.warning('Erro na requisição! Verifique a conexão com a internet.');

		})

	}

})

.controller('LoginController', function($scope, $rootScope, $location, $http, Users){

	$scope.login = function(email, password){

		Users.login(email, password).then(
		function(success){

			user = success.data;

			if(!user.error){
			 
			 $rootScope.intern_user = user.data;

			 // Modifica os headers para autenticar a requisição
			 $http.defaults.headers.common.ident = $rootScope.intern_user.id;
  			 $http.defaults.headers.common.token = $rootScope.intern_user.api_token;
  			 $http.defaults.headers.common.type = 'USER';


			 $location.path('/');


			 $rootScope.isLogged = true;

			}else{
				toastr.warning('Usuário não encontrado ou inativo! Verifique.');
			}

		},
		function(error){

			toastr.warning('Erro na requisição! Verifique a conexão com a internet.');

		})

	}

})