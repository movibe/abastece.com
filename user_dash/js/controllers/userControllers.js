angular.module('admin.controllers')

.controller('UsersController', function($scope, $rootScope, $http, $location, Users, Pagination, Requisition){

	$scope.loading = true;
	$rootScope.menuActive = 'users';
	var page_limit = Pagination.getPageLimit();
	Requisition.setModel('user');

	$scope.allUsers = function(max, offset, description){

		$scope.loading = true;

		users = Users.all(max, offset, description, $rootScope.intern_user.enterprises);
		users.then(function(success){

			$scope.loading = false;
			
			if(!success.data.error){
				Pagination.setPages(success.data.table_count);
				$scope.users = success.data.data;
			}else{
				toastr.error('Erro ao buscar o usuário!')
			}

			
		
		}, function(error){

			$scope.loading = false;
			toastr.error('Erro na requisição! Verifique a conexão com a internet.');
		});

	}

	Pagination.resetPagination();
	$scope.allUsers(Pagination.getPageLimit(), 0);

	$scope.search = function(max, offset, description){

		Pagination.resetPagination();
		$scope.allUsers(max, offset, description);

	}

	$scope.goToPage = function(mode){

		switch(mode){
			case 'next':
				var offset = Pagination.nextPage();
				$scope.allUsers(page_limit, offset);
				break;
			case 'previous':
				var offset = Pagination.previousPage();
				$scope.allUsers(page_limit, offset);
				break;
			case 'first':
				var offset = Pagination.firstPage();
				$scope.allUsers(page_limit, offset);
				break;
			case 'last':
				var offset = Pagination.lastPage();
				$scope.allUsers(page_limit, offset);
				break;
		}

	}

	$scope.new = function(){

		$location.path('/user');

	}

	$scope.edit = function(id){
		$location.path('/user/' + id);
	}

	$scope.delete = function(id){
		
		if(confirm('Tem certeza que deseja deletar esse registro?')){
			Users.delete(id).then(
			function(success){

				if(!success.data.error){
					offset = Pagination.refreshPage();
					$scope.allUsers(page_limit, offset);
				}else{
					console.log(success.data.message)
					toastr.warning(success.data.message)
				}
				
			},
			function(error){
				toastr.error('Erro na requisição! Verifique a conexão com a internet.');
			})
		}

	}

})

.controller('UserController', function($scope, $rootScope, Users, $location, $routeParams, Requisition, Upload){

	$rootScope.menuActive = 'users';
	Requisition.setModel('user');

	$scope.$watch('files', function () {
        $scope.upload($scope.files);
    });

    $scope.upload = function (files) {
        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                Upload.upload({
                    url: Requisition.getFullURL('user') + '/' + $routeParams.id + '/image/upload',
                    fields: {},
                    file: file
                }).progress(function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
                }).success(function (data, status, headers, config) {
                    console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
                });
            }
        }
    };

	// Se recebeu um id, carrega o usuário
	if($routeParams.id){

		Users.get($routeParams.id).then(
		function(success){

			if(!success.data.error){
				$scope.user = success.data.data;
				// Por conta do mysql, que não possue valores false/true
				$scope.user.active = ($scope.user.active == 1 || $scope.user.active == true) ? true : false;

				// A seleção inicial com as formas de pagamento da empresa
				$scope.selection = $scope.user.enterprises;

			}else{
				console.log(success.data.message)
			}

		},
		function(error){
			toastr.error('Erro na requisição! Verifique a conexão com a internet.');
		})

	}

	$scope.save = function(user){

		$scope.saving = true;

		user.selection = $scope.selection;
		save = Users.save(user);

		save.then(function(success){

			if(!success.data.error){
				toastr.success('Salvo com sucesso!')
				$location.path('/user/' + success.data.data.id);
			}else{
				console.log(success.data.message);
				toastr.warning(success.data.message)
			}

			$scope.saving = false;

		}, function(error){

			$scope.saving = false;
			toastr.error('Erro na requisição! Verifique a conexão com a internet.')

		})

	}

	$scope.sendForm = function(user){

		// Verifica se possui ID, para salvar ou criar um novo registro
		if(user.id){

			if(user.re_password == user.password){

				// Nem sei se iremos precisar disso, deixa pra depois
				//var old_password = prompt("Digite a senha antiga:");
				$scope.save(user);

			}else{
				toastr.error('As senhas não conferem!');
			}	

		}else{

			if(user.re_password == user.password){

				$scope.save(user);

			}else{
				toastr.error('As senhas não conferem!');
			}

		}

	}

	// Trabalhando com o checkbox
	$scope.selection = [];

	$scope.isChecked = function(id){
      var match = false;
      for(var i=0 ; i < $scope.selection.length; i++) {
        if($scope.selection[i].id == id){
          match = true;
        }
      }
      return match;
  	};

  	$scope.sync = function(bool, item){
	    
	    if(bool){
	      // Adiciona o item
	      $scope.selection.push(item);
	    } else {
	      // Remove o item
	      for(var i=0 ; i < $scope.selection.length; i++) {
	        if($scope.selection[i].id == item.id){
	          $scope.selection.splice(i,1);
	        }
	      }      
	    }

  	};

})