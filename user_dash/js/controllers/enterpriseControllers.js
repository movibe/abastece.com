angular.module('admin.controllers')

.controller('CommentsController', function($scope, $rootScope, $location, $routeParams, Enterprises){

	Enterprises.getComments($routeParams.id).then(function(s){

		$scope.comments = Enterprises.resolveComments(s.data.data);

	}, function(e){

		toastr.warning('Erro na requisição! Verifique a conexão com a internet.');

	})

})

.controller('EnterpriseController', function($scope, $rootScope, Enterprises, Payments, Flags, $location, $routeParams,
 											 $http, Requisition, Upload){

	$scope.enterprise = {payments : {}, sel_payments : []};

	$scope.$watch('files', function () {
		var url = Requisition.getFullURL('enterprise') + '/' + $routeParams.id + '/image/upload'; 
        $scope.upload($scope.files, url);
    });

    $scope.upload = function (files, _url) {
        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                Upload.upload({
                    url: _url,
                    fields: {},
                    file: file
                }).progress(function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
                }).success(function (data, status, headers, config) {
                    console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
                });
            }
        }
    };
	
	$rootScope.menuActive = 'enterprises';
	Requisition.setModel('enterprise');

	// Se recebeu um id, carrega o usuário
	if($routeParams.id){

		Enterprises.get($routeParams.id).then(
		function(success){

			if(!success.data.error){
				
				$scope.enterprise 						= success.data.data;
				$scope.enterprise.hasGasoline 			= ($scope.enterprise.hasGasoline == 1 || $scope.enterprise.hasGasoline == true) ? true : false;
				$scope.enterprise.hasGasolineLeaded 	= ($scope.enterprise.hasGasolineLeaded == 1 || $scope.enterprise.hasGasolineLeaded == true) ? true : false;
				$scope.enterprise.hasGasolinePremium 	= ($scope.enterprise.hasGasolinePremium == 1 || $scope.enterprise.hasGasolinePremium == true) ? true : false;
				$scope.enterprise.hasEthanol	 		= ($scope.enterprise.hasEthanol == 1 || $scope.enterprise.hasEthanol == true) ? true : false;
				$scope.enterprise.hasDiesel 			= ($scope.enterprise.hasDiesel == 1 || $scope.enterprise.hasDiesel == true) ? true : false;
				$scope.enterprise.hasDieselS10 			= ($scope.enterprise.hasDieselS10 == 1 || $scope.enterprise.hasDieselS10 == true) ? true : false;
				$scope.enterprise.hasNaturalGas 		= ($scope.enterprise.hasNaturalGas == 1 || $scope.enterprise.hasNaturalGas == true) ? true : false;
				$scope.enterprise.active = ($scope.enterprise.active == 1 || $scope.enterprise.active == true) ? true : false;
				// Obtém os serviços
				$scope.getEnterpriseServices();

				// A seleção inicial com as formas de pagamento da empresa
				$scope.selection = $scope.enterprise.payments;

				// Obtém as formas de pagamento
				$scope.getPaymentsList();

			}else{
				console.log(success.data.message)
			}

		},
		function(error){
			toastr.error('Erro na requisição! Verifique a conexão com a internet.');
		})

	}

	$scope.save = function(enterprise, mode){

		enterprise.selection = $scope.selection;

		Enterprises.save(enterprise, mode)
		.then(function(success){

			if(!success.data.error){
				toastr.success('Salvo com sucesso!')
				$location.path('/' + mode + '/' + success.data.data.id);
			}else{
				console.log(success.data.message);
				toastr.error(success.data.message)
			}

		}, function(error){

			toastr.error('Erro na requisição! Verifique a conexão com a internet.')

		})

	}

	$scope.sendForm = function(enterprise, mode){

		mode = (!mode) ? 'enterprise' : mode;
		$scope.save(enterprise, mode);

	}

	$scope.getCoordinates = function(address){

		if(address){

			street = address.street.replace(' ', '+');

			url = 'https://maps.google.com/maps/api/geocode/json?address='+address.number+'+'
					+street+'+'+address.city+'+'+address.country+'&sensor=false';

			$http({
			    method: 'GET',
			    url: url,
			    // Aqui deletamos os headers para utilizar essa requisição externa
			    transformRequest: function(data, headersGetter) {
			        var headers = headersGetter();

			        delete headers['ident'];
			        delete headers['token'];
			        delete headers['type'];

			        return headers;
			    }
			}).then(
			function(success){
				
				data = success.data;

				if(data.results.length > 0){
					loc = data.results[0].geometry.location;

					$scope.enterprise.latitude  = loc.lat;
					$scope.enterprise.longitude = loc.lng;

					toastr.info('Coordenadas encontradas ' + address.street + ', ' + address.number)
				
				}else{

					$scope.enterprise.latitude  = 0;
					$scope.enterprise.longitude = 0;

					toastr.info('Não existem coordenadas para ' + address.street + ', ' + address.number)

				}
				
			},
			function(error){
				toastr.error('Erro na requisição! Verifique a conexão com a internet.')
			})

		}

	}

	$scope.getPaymentsList = function(){

		Requisition.setModel('payment');
		Payments.all().
		then(function(success){

			$scope.payments_list = success.data.data;
			Requisition.setModel('enterprise');

		}, function(error){
			console.log('Erro ao carregar os endereços');
			Requisition.setModel('enterprise');
		})

	}

	$scope.allFlags = function(){

		// Seta que vai trabalhar com o model de flags
		Requisition.setModel('flag');
		
		Flags.all()
		.then(function(success){

			if(!success.data.error){
				
				$scope.flags = success.data.data;
				$scope.flags.map(function(el){
					el.name = (el.name_anp) ? el.name + ' - ' + el.name_anp : el.name;
				})

			}else{
				toastr.error('Erro ao buscar as bandeiras!')
			}
		
		}, function(error){

			toastr.error('Erro na requisição! Verifique a conexão com a internet.');

		});

		// Retorna para o model da promoção
		Requisition.setModel('enterprise');

	}

	// Obtém as bandeiras 
	$scope.allFlags();

	$scope.getEnterpriseServices = function(){

		Enterprises.getEnterpriseServices($routeParams.id).
		then(function(success){

			$scope.services = success.data.data;

		}, function(error){
			console.log('Erro ao carregar os serviços');
		})

	}

	$scope.sendService = function(service){

		Enterprises.saveService(service, $routeParams.id)
		.then(function(success){

			$scope.service_mode = 'listing';
			$scope.service = {};
			$scope.getEnterpriseServices();
			toastr.success('Serviço salvo com sucesso!')

		}, function(error){

			toastr.error('Erro na requisição! Verifique a conexão com a internet.')

		})

	}

	$scope.newService = function(){

		$scope.service_mode = 'editing';
		$scope.service = {};

	}

	$scope.editService = function(service){

		$scope.service_mode = 'editing';
		$scope.service = service;

	}

	$scope.deleteService = function(id){

		if(confirm("Deseja excluir este serviço?")){
		
			Enterprises.deleteService(id)
			.then(function(success){
	
				$scope.getEnterpriseServices();
	
			}, function(error){
	
				toastr.error('Erro na requisição! Verifique a conexão com a internet.')
	
			})
		}
	}

	// Trabalhando com o checkbox
	$scope.selection = [];

	$scope.isChecked = function(id){
      var match = false;
      for(var i=0 ; i < $scope.selection.length; i++) {
        if($scope.selection[i].id == id){
          match = true;
        }
      }
      return match;
  	};

  	$scope.sync = function(bool, item){
	    if(bool){
	      // Adiciona o item
	      $scope.selection.push(item);
	    } else {
	      // Remove o item
	      for(var i=0 ; i < $scope.selection.length; i++) {
	        if($scope.selection[i].id == item.id){
	          $scope.selection.splice(i,1);
	        }
	      }      
	    }

	  };

})
