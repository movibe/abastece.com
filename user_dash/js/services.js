angular.module('admin-services', [])

.factory('Users', function($rootScope, $http, Requisition){

	return {

		all : function(max, offset, desc, enterprises){
			
			return $http.post(Requisition.getFullURL('users') + '/editor', {enterprises: enterprises});

		},

		get : function(id){

			return Requisition.get(id);

		},

		getEnterprises : function(user){
			
			return $http.get(Requisition.getFullURL('user') + '/' + user + '/enterprises');

		},

		save : function(data){

			return Requisition.save(data);
			

		},

		delete : function(id){

			return Requisition.delete(id);

		},

		login : function(email, password){
			
			return $http.post(Requisition.getFullURL('user') + '/login/editor', 
			{'email' : email, 'password' : password});

		}

	}

})

.factory('Enterprises', function($rootScope, $http, Requisition){

	return {

		all : function(max, offset, desc){
			
			return Requisition.all(max, offset, desc);

		},

		get : function(id){

			return Requisition.get(id);

		},

		save : function(data, mode){

			// Se possui um ID vai salvar, caso contrário vai criar um novo registro
			if(data.id){
				return $http.put(Requisition.getFullURL('enterprise') +'/' + data.id + '?mode=' + mode, data);
			}else{
				return $http.post(Requisition.getFullURL('enterprises'), data);
			}
			

		},

		delete : function(id){

			return Requisition.delete(id);

		},

		saveService : function(data, enterprise_id){

			if(data.id){

				return $http.put(Requisition.getFullURL('enterprise') + '/service/' + data.id, data);

			}else{

				return $http.post(Requisition.getFullURL('enterprise') + '/' + enterprise_id + '/services', data);

			}	

		},

		deleteService : function(id){

			return $http.delete(Requisition.getFullURL('enterprise') + '/service/' + id);

		},

		getEnterpriseServices : function(enterprise_id){

			return $http.get(Requisition.getFullURL('enterprise') + '/' + enterprise_id + '/services');

		},

		resolveComments : function(data){

	      data.map(function(el){

	        el.updated_at = mysqlTimeToInput(el.updated_at);

	      });

	      return data;

	    },

	    getComments : function(enterprise){

	    	return $http.get(Requisition.getFullURL('enterprise') + '/' + enterprise + '/comments');

	    }

	}

})

.factory('Payments', function($rootScope, $http, Requisition){

	return {

		all : function(max, offset, desc){
			
			return Requisition.all(max, offset, desc);

		},

		get : function(id){

			return Requisition.get(id);

		},

		save : function(data){
			
			return Requisition.save(data);
			
		},

		delete : function(id){

			return Requisition.delete(id);

		}

	}

})

.factory('Flags', function($rootScope, $http, Requisition){

	return {

		all : function(max, offset, desc){
			
			return Requisition.all(max, offset, desc);

		},

		get : function(id){

			return Requisition.get(id);

		},

		save : function(data){
			
			return Requisition.save(data);
			
		},

		delete : function(id){

			return Requisition.delete(id);

		}

	}

})