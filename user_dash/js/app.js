angular.module('admin', ['ngRoute', 'ngResource', 'ngFileUpload', 'admin.controllers',
			   			 'admin-services', 'pagination', 'requisition', 'ngMask'])

.run(function($rootScope, Pagination, Requisition, $templateCache){

	// Limpa o cache sempre que carrega uma View
	$rootScope.$on('$viewContentLoaded', function() {
      $templateCache.removeAll();
   	});

	Requisition.setURL('http://localhost:8080/abastece_com/api/');

	//Paginação
	Pagination.setPageLimit(10);
	Pagination.resetPagination();

	//Qual elemento está ativo para uso do menu
	$rootScope.menuActive = '';

	// Configurações do TOAST
	toastr.options = {
	  "closeButton": true,
	  "debug": false,
	  "newestOnTop": false,
	  "progressBar": false,
	  "positionClass": "toast-top-right",
	  "preventDuplicates": false,
	  "onclick": null,
	  "showDuration": "300",
	  "hideDuration": "500",
	  "timeOut": "2500",
	  "extendedTimeOut": "500",
	  "showEasing": "swing",
	  "hideEasing": "linear",
	  "showMethod": "fadeIn",
	  "hideMethod": "fadeOut"
	}

})

.config(function($routeProvider){

	$routeProvider
	.when('/', {
		resolve: {
			'check' : function($rootScope, $location){

				if(!$rootScope.isLogged){
					$location.path('/login');
				}

			 }
		},
		templateUrl : 'pages/home.html',
		controller : 'HomeController'
	})
	.when('/login', {
		templateUrl : 'login.html',
		controller : 'LoginController'
	})
	.when('/block', {
		templateUrl : 'block.html'
	})
	.when('/prices/:id?', {
		resolve: {
			'check' : function($rootScope, $location){

				if(!$rootScope.isLogged){
					$location.path('/login');
				}

			 }
		},
		templateUrl : 'pages/prices.html',
		controller : 'EnterpriseController'
	})
	.when('/services/:id?', {
		resolve: {
			'check' : function($rootScope, $location){

				if(!$rootScope.isLogged){
					$location.path('/login');
				}

				if($rootScope.isLogged && $rootScope.intern_user.type != 'ADMIN' && $rootScope.intern_user.type != 'EDITOR'){
					$location.path('/block');
				}

			 }
		},
		templateUrl : 'pages/services.html',
		controller : 'EnterpriseController'
	})
	.when('/payments/:id?', {
		resolve: {
			'check' : function($rootScope, $location){

				if(!$rootScope.isLogged){
					$location.path('/login');
				}

				if($rootScope.isLogged && $rootScope.intern_user.type != 'ADMIN' && $rootScope.intern_user.type != 'EDITOR'){
					$location.path('/block');
				}

			 }
		},
		templateUrl : 'pages/payments.html',
		controller : 'EnterpriseController'
	})
	.when('/comments/:id?', {
		resolve: {
			'check' : function($rootScope, $location){

				if(!$rootScope.isLogged){
					$location.path('/login');
				}

				if($rootScope.isLogged && $rootScope.intern_user.type != 'ADMIN' && $rootScope.intern_user.type != 'EDITOR'){
					$location.path('/block');
				}

			 }
		},
		templateUrl : 'pages/comments.html',
		controller : 'CommentsController'
	})
	.when('/enterprise/:id?', {
		resolve: {
			'check' : function($rootScope, $location){

				if(!$rootScope.isLogged){
					$location.path('/login');
				}

				if($rootScope.isLogged && $rootScope.intern_user.type != 'ADMIN' && $rootScope.intern_user.type != 'EDITOR'){
					$location.path('/block');
				}

			 }
		},
		templateUrl : 'pages/enterprise.html',
		controller : 'EnterpriseController'
	})
	.when('/users', {
		resolve: {
			'check' : function($rootScope, $location){

				if(!$rootScope.isLogged){
					$location.path('/login');
				}

				if($rootScope.isLogged && $rootScope.intern_user.type != 'ADMIN' && $rootScope.intern_user.type != 'EDITOR'){
					$location.path('/block');
				}

			 }
		},
		templateUrl : 'pages/users.html',
		controller : 'UsersController'
	})
	.when('/user/:id?', {
		resolve: {
			'check' : function($rootScope, $location){

				if(!$rootScope.isLogged){
					$location.path('/login');
				}

				if($rootScope.isLogged && $rootScope.intern_user.type != 'ADMIN' && $rootScope.intern_user.type != 'EDITOR'){
					$location.path('/block');
				}

			 }
		},
		templateUrl : 'pages/user.html',
		controller : 'UserController'
	})
	.otherwise({
        redirectTo: '/'
    });

	//$locationProvider.html5Mode(true);

})