angular.module('requisition', [])

.service('Requisition', function($http){

    var model = '';
    var url   = '';

	return {

		setModel : function(_model){

			model = _model;

		},

		setURL : function(_url){

			url = _url;

		},

		getURL : function(){

			return url;

		},

		getFullURL : function(model, version){

			version = version ? version : 'v1';

			return url + version + '/' + model;

		},

		all : function(max, offset, desc, version, order_by){
			
			limit       = (max) ? max : '';
			offset      = offset ? offset : '';
			description = (desc) ? desc : '';
			version     = version ? version : 'v1';
			order_by    = order_by ? order_by : 'id';

			return $http.get(url + version+'/'+model+'s?description=' + description + '&limit=' + limit + '&skip=' + offset
				 			+ '&order=' + order_by);

		},

		// Retorna todos exceto um determinado id, o order by é o segundo parâmetro pois será mais utilizada em selects
		allExcept : function(except, order_by, max, offset, desc, version){
			
			limit       = (max) ? max : '';
			offset      = offset ? offset : '';
			description = (desc) ? desc : '';
			version     = version ? version : 'v1';
			except      = except ? except : '0';
			order_by    = order_by ? order_by : 'id';

			return $http.get(url + version + '/'+model+'s?except=' + except + '&description=' + description 
							+ '&limit=' + limit + '&skip=' + offset + '&order=' + order_by);

		},

		get : function(id, version){

			version = version ? version : 'v1';
			return $http.get(url + version+'/'+model+'/'+id);

		},

		save : function(data, version){

			version = version ? version : 'v1';

			// Se possui um ID vai salvar, caso contrário vai criar um novo registro
			if(data.id){
				return $http.put(url + version+'/'+model+'/'+data.id, data);
			}else{
				return $http.post(url + version+'/'+model+'s', data);
			}
			

		},

		delete : function(id, version){

			version = version ? version : 'v1';

			return $http.delete(url + version+'/'+model+'/'+id);

		}

	}

})