angular.module('pagination', [])

.service('Pagination', function($rootScope){

	return {

		resetPagination : function(){

			$rootScope.first  = 1;
			$rootScope.second = 2;
			$rootScope.last   = 1;
			$rootScope.actual = 1;
			$rootScope.pages  = 1;
		
		},

		setPageLimit : function(limit){

			$rootScope.limit = limit;

		},

		getPageLimit : function(){

			return $rootScope.limit;

		},

		// Recebe a quantidade total de registros, compara com o limite para cada página e gera o total de páginas
		// para a paginação
		setPages : function(count){

			// Quantidade de páginas
			count = parseInt(count, 10);

			if(count > $rootScope.limit){

				// Testa se ao dividir a quantidade de páginas pelo limite o resto é 0
				if((count % $rootScope.limit) == 0){
					$rootScope.pages = (count / $rootScope.limit);
				}else{
					$rootScope.pages = (count / $rootScope.limit) + 1;
				}

			}else{
				$rootScope.pages = 1;
			}

			$rootScope.pages = parseInt($rootScope.pages, 10);
			$rootScope.last = $rootScope.pages;

		},

		nextPage : function(){

			$rootScope.actual = $rootScope.actual + 1;
			$rootScope.first  = $rootScope.actual;
			$rootScope.second = $rootScope.first + 1;

			return (($rootScope.actual - 1) * this.getPageLimit());

		},

		previousPage : function(){

			$rootScope.actual = $rootScope.actual - 1;
			$rootScope.first  = $rootScope.actual;
			$rootScope.second = $rootScope.first + 1;

			return (($rootScope.actual - 1) * this.getPageLimit());

		},

		firstPage : function(){

			$rootScope.actual = 1;
			$rootScope.first  = $rootScope.actual;
			$rootScope.second = $rootScope.first + 1;

			return 0;

		},

		lastPage : function(){

			$rootScope.actual = $rootScope.last;
			$rootScope.first  = $rootScope.actual;

			return (($rootScope.actual - 1) * this.getPageLimit());

		},

		refreshPage : function(){

			return (($rootScope.actual - 1) * this.getPageLimit());

		}

	}

})